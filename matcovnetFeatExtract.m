function matcovnetFeatExtract()

    addpath '/home/sareh/Codes/matconvnet/matlab';
    vl_setupnn();
    
    net = dagnn.DagNN.loadobj(load('/home/sareh/Codes/matconvnet/models/imagenet-resnet-152-dag.mat')) ;
    net.mode = 'test' ;
    net.conserveMemory = false;
    MAIN_DATA = '/home/sareh/Datasets/IKEA/data/'; 
    runs = dir(MAIN_DATA);
    
    for run = 3 : numel(runs)        
        imgs = dir(sprintf('%s/*.jpg',fullfile(MAIN_DATA,runs(run).name)));
        file = fullfile('/home/sareh/Datasets/IKEA/matcovnetFeats',sprintf('%s.mat',runs(run).name));
        if exist(file,'file') == 2
            continue;
        end
        feats = zeros(numel(imgs),2048,'single');
        for i = 1 : numel(imgs)
            im = imread(fullfile(MAIN_DATA,runs(run).name,imgs(i).name));           
            im_ = single(im) ; % note: 0-255 range
            im_ = imresize(im_, net.meta.normalization.imageSize(1:2)) ;
            im_ = bsxfun(@minus, im_, net.meta.normalization.averageImage) ;    
            net.eval({'data', im_}) ;
            feats(i,:) = squeeze(net.vars(net.getVarIndex('pool5')).value) ;
            fprintf('run %s img %d of %d \n',runs(run).name,i,numel(imgs));
        end
        save(file,'feats');
    end

   

end