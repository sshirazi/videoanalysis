function annotations = getCookingGTLablesForVIdeo(dataset,id)
    IndexC = strfind(dataset.clipnames, dataset.videonames{id});
    Index = find(not(cellfun('isempty', IndexC)));
    annotations = dataset.clipannotations(Index,:);
end