function data = loadCookingSequences(dataset,id)    
    load(fullfile(dataset.bowfeatdir,sprintf('%s.mat',dataset.matnames{id})));
    %obj = VideoReader(fullfile(dataset.dbpath,'videos',sprintf('%s.avi',dataset.videonames{id})));
    nFrames  = size(integralFrameHist,2);
    data = zeros(size(integralFrameHist),'single');
    for i = 2 : nFrames
        data(:,i) = integralFrameHist(:,i) - integralFrameHist(:,i-1);
    end
    data(:,1) = integralFrameHist(:,1);
    data  = data';
end