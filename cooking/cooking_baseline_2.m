
%
% This baseline just apply rank pooling and do the matching without any
% temporal consistency search
%
%
function cooking_baseline_2(varargin)

opts.feat = 'hof';
opts.issareh = false;
opts.window  = 91;
opts.stride  = 10;
opts.poolmethod = 'rank';
opts.maxdetection = 100;
opts.threshold = 0.2;
opts.prepoolnonlinear = 'none';
opts = bc_argparse(opts,varargin);
STR = getOptsString(opts);

dataset = getCookingDataset(opts.issareh,'hof');       
pair_id = 1;







if opts.isnci
    MAIN_DIR = '/short/w87/bxf660/Data';        
    results_file = sprintf('../detections/baseline2/res%s.mat',STR);
else
    MAIN_DIR = '/data/home/basura/Cooking_EXperiment_data';
    results_file = sprintf('../Cooking_EXperiment_data/detections/baseline2/res%s.mat',STR);
end       

DATA_FILE = sprintf('%s/Cooking_CVPR17_Seq_Data/Feat-%s-Window%d-Stride%d-Pool%s-Rank%s.mat',...
        MAIN_DIR,opts.feat,opts.window,opts.stride,opts.prepoolnonlinear,opts.poolmethod);

    
matObj = matfile(DATA_FILE);
VID = matObj.VID;

NN = numel(dataset.matnames) ;

THRESH = [0.1 0.2 0.3 0.4 0.5];
precision = zeros(NN*(NN-1)/2,5);
recall = zeros(NN*(NN-1)/2,5);

for seq_id1 = 1 : numel(dataset.matnames) 
    
    rankpool1 = matObj.data(find(VID == seq_id1),:);    
    gt1 = getCookingGTLablesForVIdeo(dataset,seq_id1);    
    nDimsX = getCookingSequenceSize(opts.issareh,seq_id1);
    range1 = (opts.window-1)/2+1 : opts.stride : nDimsX -(opts.window-1)/2;
    
    for seq_id2 =  seq_id1+1: numel(dataset.matnames) 
        timest = tic();        
        
        nDimsX = getCookingSequenceSize(opts.issareh,seq_id2);
        range2 = (opts.window-1)/2+1 : opts.stride : nDimsX-(opts.window-1)/2;
        rankpool2 = matObj.data(find(VID == seq_id2),:);
        gt2 = getCookingGTLablesForVIdeo(dataset,seq_id2);
        
        [pr1,pr2] = matchTemporalSequences(rankpool1,rankpool2,opts.maxdetection,opts.window, range1, range2 );
        
        for THi = 1 : numel(THRESH)
                [correctdetections,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,1,THRESH(THi));            
                precision(pair_id,THi) = size(correctdetections,1)/size(gtdetection,1);
                recall(pair_id,THi) = size(correctdetections,1)/size(pr1,1);                    
        end        
      
        timest = toc(timest);
        
        fprintf('[Matching][W%d-S%d-D%d][pair %d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
            opts.window,opts.stride,opts.maxdetection,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
            mean(precision(1:pair_id,1)*100),mean(recall(1:pair_id,1)*100),timest);                                    
        if mod(pair_id,25) == 0
            save(results_file,'precision','recall','opts');  
        end
        pair_id = pair_id + 1;                                 
    end        
end
save(results_file,'precision','recall','opts');  

function S = getOptsString(opt)
    opts = opt;
    opts = rmfield(opts,'issareh');
    names = fieldnames(opts);
    S = '';
    for n = names'
        value = getfield(opts, n{1});
        ss = n{1};
        S = sprintf('%s-%s',S,ss(1));
        if ischar(value)
            S = [S '-' value ];
        end
        if isnumeric(value)
            S = [S '-' num2str(value) ];
        end
    end
    fprintf('%s\n',S);
