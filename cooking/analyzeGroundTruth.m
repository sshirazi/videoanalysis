%
% The objective of this experiment is to evaluate the qulity of pooling
% methods for detection task.
% 
% We get pairs of ground truth segments and compare them using temporal pooled
% detection similarity.
%
function [score,totalGt] = analyzeGroundTruth()
opts.feat = 'hof';
opts.issareh = false;

dataset = getCookingDataset(opts.issareh,opts.feat);       

id = 1;
totalGt = [];
for seq_id1 = 1 : numel(dataset.matnames)        
    gt1 = getCookingGTLablesForVIdeo(dataset,seq_id1);            
    data1 = loadCookingSequences(dataset,seq_id1) ;    
    for seq_id2 =  seq_id1+1: numel(dataset.matnames) 
        gt2 = getCookingGTLablesForVIdeo(dataset,seq_id2);            
        data2 = loadCookingSequences(dataset,seq_id2) ;   
        CID = unique([gt1(:,3) ; gt2(:,3)]);
        CID(find(CID == 1)) = [];        
        for cl = 1 : numel(CID)
            if numel(find(gt1(:,3) == CID(cl) )) > 0 && numel(find(gt2(:,3) == CID(cl) )) > 0
                clip_indxs1 = find(gt1(:,3) == CID(cl));
                clip_indxs2 = find(gt2(:,3) == CID(cl));   
                set1 = gt1(clip_indxs1,:);
                set2 = gt2(clip_indxs2,:);
                for c1 = 1 : numel(clip_indxs1)
                    dt1 = data1(set1(c1,1):min(set1(c1,2),size(data1,1)),:);
                    urp1 = genRepresentation(dt1,1,'chi2exp');
                    urp1 = urp1./norm(urp1);
                    for c2 = 1 : numel(clip_indxs2)                       
                        dt2 = data2(set2(c2,1):min(set2(c2,2),size(data2,1)),:);
                        urp2 = genRepresentation(dt2,1,'chi2exp');
                        urp2 = urp2./norm(urp2);
                        totalGt = [totalGt ; set1(c1,:) set2(c2,:)];      
                        score(id) = urp1'*urp2;                        
                        distance(id) = norm(urp1-urp2);
                        id = id + 1;
                        fprintf('[seq1 %d][seq2 %d][matchid %d][score %1.3f][dist %1.3g]\n',...
                            seq_id1,seq_id2,id-1,score(id-1),distance(id-1));
                    end
                end        
            end
        end
        save('score-analysis.mat','score','totalGt','distance');
    end
end
 