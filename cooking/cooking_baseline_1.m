%
%
% Clusterin based baseline.
%
%
function [expconfig] = cooking_baseline_1()   
    opts = getOptimalConfiguration();
    [pr,rc,F] = main('clusternonlinearity',opts.clusternonlinearity,...
                                'clusterNorm',opts.clusterNorm,...
                                'poolnonlinear',opts.poolnonlinears,...
                                'poolnorm',opts.poolnorms,'poolmethod',opts.poolmethods,'islite',false);       

end

function opts = getOptimalConfiguration()
    clusternonlinearitys = {'none','ssr','chi1','chi2'};
    clusterNorms = {'none','L2'} ;
    poolnonlinears = {'none','ssr','chi1','chi2'};
    poolnorms = {'none','L2'} ;
    poolmethods = {'apr','rank'};
    id = 1;
    load('expconfig');
    for clusternonlinearity = clusternonlinearitys
        for clusterNorm = clusterNorms
            for poolnonlinear = poolnonlinears
                for poolnorm = poolnorms
                    for poolmethod = poolmethods
                        if (id-1)~= size(expconfig,2)
                            id = id + 1;
                        else
                            [pr,rc,F] = main('clusternonlinearity',clusternonlinearity{1},...
                                'clusterNorm',clusterNorm{1},...
                                'poolnonlinear',poolnonlinear{1},...
                                'poolnorm',poolnorm{1},'poolmethod',poolmethod{1});                    
                            expconfig(id).clusternonlinearity = clusternonlinearity{1};
                            expconfig(id).clusterNorm = clusterNorm{1};
                            expconfig(id).poolnonlinears = poolnonlinear{1};
                            expconfig(id).poolnorms = poolnorm{1};
                            expconfig(id).poolmethods = poolmethod{1};
                            expconfig(id).recall = rc;
                            expconfig(id).precision = pr;
                            expconfig(id).fscore = F;
                            save('expconfig.mat','expconfig');
                            id = id + 1;
                        end
                    end
                end
            end
        end
    end
    for i = 1 :numel(expconfig) 
        f(i) = expconfig(i).fscore;
    end
    [val,indx] = max(f);
    
    opts.clusternonlinearity=  expconfig(indx).clusternonlinearity;
    opts.clusterNorm = expconfig(indx).clusterNorm;
    opts.poolmethods = expconfig(indx).poolmethods;
    opts.poolnonlinears = expconfig(indx).poolnonlinears;
    opts.poolnorms = expconfig(indx).poolnorms;
    
end

function [pr,rc,F] = main(varargin)

opts.feat           = 'mbh';
opts.issareh        = false;

opts.numclusters    = 10;
opts.clusternonlinearity = 'none';
opts.clusterNorm         = 'none' ;
opts.window         = 61;
opts.coef           = 0.001;
opts.theshlen       = 60;

opts.poolmethod     = 'apr';
opts.poolnonlinear  = 'none';
opts.poolnorm       = 'none' ;

opts.maxdetection   = 400;
opts.islite         = true;

opts = bc_argparse(opts,varargin);

opts.pool.method    = opts.poolmethod;
opts.pool.nonlinear = opts.poolnonlinear;
opts.pool.norm      = opts.poolnorm    ;

opts.endvid = inf;
if opts.islite
    opts.endvid = 5;
end
opts
opts.pool

dataset             = getCookingDataset(opts.issareh,opts.feat);  
allcandidates = generateCandidates(opts,dataset);
[pr,rc,F] = matchVideos(allcandidates,dataset,opts);
fprintf('*******************************\n');
for i = 1 : numel(pr)
    fprintf('& \t %1.1f & \t %1.1f & \t %1.1f \\\\ \n',pr(i)*100,rc(i)*100,F(i)*100);
end

end

function [pr,rc,F] = matchVideos(allcandidates,dataset,opts)
    resDir = '../Cooking_EXperiment_data/detections/baseline_1';
    resFile = fullfile(resDir,sprintf('%s-C%d-%s-%s-W%d-coef%1.4f-threshL%d-%s-%s-%s-%d.mat',...
    opts.feat, opts.numclusters,opts.clusternonlinearity,opts.clusterNorm,...
        opts.window,opts.coef,opts.theshlen,opts.poolmethod,opts.poolnonlinear,...
        opts.poolnorm,opts.maxdetection));
    
    res = load(resFile);
    if res.precision(end,1) ~= 0
        pr = mean(res.precision);
        rc = mean(res.recall);
        F  = 2 * (pr .* rc) ./ (pr + rc);  
        return;
    end
    
    pair_id = 1;  
    NN = min(numel(dataset.matnames),opts.endvid)  ;
    
    
    THRESH = [0.1 0.2 0.3 0.4 0.5];
    
    precision = zeros(NN*(NN-1)/2,5);
    recall = zeros(NN*(NN-1)/2,5);
    
    
    
    for seq_id1 = 1 : min(numel(dataset.matnames),opts.endvid)        
        gt1 = getCookingGTLablesForVIdeo(dataset,seq_id1);                    
        out1 = getVideoEncodings(loadCookingSequences(dataset,seq_id1),allcandidates{seq_id1},opts);
        for seq_id2 =  seq_id1+1: min(opts.endvid,numel(dataset.matnames)) 
            timest = tic();
            gt2 = getCookingGTLablesForVIdeo(dataset,seq_id2);                        
            fprintf('.');
            out2 = getVideoEncodings(loadCookingSequences(dataset,seq_id2),allcandidates{seq_id2},opts);
            fprintf('.');
            M = out1 * out2';
            [pr1, pr2] = getDetections(M, allcandidates{seq_id1}, allcandidates{seq_id2}, opts.maxdetection) ;
            fprintf('.');
            for THi = 1 : numel(THRESH)
                [correctdetections,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,1,THRESH(THi));            
                precision(pair_id,THi) = size(correctdetections,1)/size(gtdetection,1);
                recall(pair_id,THi) = size(correctdetections,1)/size(pr1,1);                    
            end
            
            
            timest = toc(timest);
            fprintf('[BS1][W%d-D%d][pair %d vs %d][ Prec. %1.3f][ Rec. %1.3f][Mean Pr. %1.3f][Mean Rc. %1.3f][F = %1.3f][F mean = %1.3f][Time %f]\n',...
            opts.window,opts.maxdetection,seq_id1,seq_id2,precision(pair_id,5)*100,recall(pair_id,5)*100,...
            mean(precision(1:pair_id,5)*100),mean(recall(1:pair_id,5)*100), ...
            (200 * precision(pair_id,5) * recall(pair_id,5) / (recall(pair_id,5) + precision(pair_id,5)) ),...
            200* mean(precision(1:pair_id,5)) * mean(recall(1:pair_id,5))/ (mean(precision(1:pair_id,5)) + mean(recall(1:pair_id,5)))  ,timest);   
            if ~opts.islite 
                save(resFile,'precision','recall');
            end
            pair_id = pair_id + 1;
        end
    end    
    
    pr = mean(precision(:,5)*100);
    rc = mean(recall(:,5)*100);
    F = 200* mean(precision(:,5)) * mean(recall(:,5))/ (mean(precision(:,5)) + mean(recall(:,5)));
    fprintf('[Mean Pr. %1.3f][Mean Rc. %1.3f][F mean = %1.3f]\n',...            
            pr,rc,F );  
    
end
function [pr1, pr2] = getDetections(A,Candidate1,Candidate2,max_detections)   
    max_detections = min(max_detections,numel(find(A(:)>0.2)));
    pr1 = zeros(max_detections,2);
    pr2 = zeros(max_detections,2);
    for detection_id = 1 : max_detections
                [val,I] = max(A(:));                
                [I_row, I_col] = ind2sub(size(A),I);
                % get detection for video 1                
                pr1(detection_id,:) = Candidate1(I_row,:);                
                % get detection for video 2               
                pr2(detection_id,:) = Candidate2(I_col,:);                
                A(I_row,I_col) = -inf;
    end
end

function out = getVideoEncodings(data,candidates,opts)
    switch opts.pool.nonlinear
        case {'none','ssr'}
            dim = 2;
        case 'chi2exp'
            dim = 12;   
        case 'chi1'
            dim = 6;       
        case 'chi2'
            dim = 10;       
    
    end

    out = zeros(size(candidates,1),size(data,2)*dim,'single');
    for i = 1 : size(candidates,1)
        seq = data(candidates(i,1):candidates(i,2),:); 
        switch opts.pool.method
            case 'apr'
                out(i,:) = genAprRepresentation(seq,1,opts.pool.nonlinear);
            case 'rank'
                out(i,:) = genRepresentation(seq,1,opts.pool.nonlinear);
        end
%         if mod(i,80) == 0
%             fprintf('\n');
%         else
%             fprintf('.');
%         end
    end
%    fprintf('\n');    
    switch opts.pool.norm 
        case 'L2'
            out = normalizeL2(out);
        case 'none'
            out = out;
        case 'default'
            out = out;
    end
            
end

function allcandidates = generateCandidates(opts,dataset)
at = harmonic(1:opts.window);
M = repmat(at',1,4000);
if opts.islite
    sfx = 'lite';
else
    sfx = '';
end
FILE = sprintf('candiadates/%s%s-%s-Norm-feat%s-C%d-W%d-L%d.mat',...
    sfx,opts.clusternonlinearity,opts.clusterNorm,opts.feat,opts.numclusters,...
    opts.window,opts.theshlen );
st = 1;
if exist(FILE,'file')==2
    load(FILE);    
    st = numel(allcandidates)+ 1;
end
for seq_id1 = st : min(opts.endvid,numel(dataset.matnames))                       
    data = loadCookingSequences(dataset,seq_id1) ;          
    data = tempPool(data,M);    
    data = getNonLinearity(data,opts.clusternonlinearity);
    optsval = 0;
    switch opts.clusterNorm
        case 'L2'
            data = normalizeL2(data);
            optsval = 1;
        case 'none'
            temp = data * data';
            optsval = mean(temp(:)); clear temp;
    end
    data = [data opts.coef*optsval/size(data,1)*(1:size(data,1))'];
    [~, idxkmean] = vl_kmeans(data' , opts.numclusters, 'algorithm', 'elkan', 'MaxNumIterations', 100) ;     
    changes = getChange(idxkmean);
    ends = find(changes~=0);
    detections = zeros(numel(ends)+1,2);
    for i = 1 : numel(ends)
        if i == 1
            detections(i,1) = 1;
            detections(i,2) = ends(i);
        else
            detections(i,1) = ends(i-1)+1;
            detections(i,2) = ends(i);
        end
    end    
    detections(end+1,1:2) = [ends(i) numel(idxkmean)];    
    candaidates = [];
    for i = 1 : size(detections,1)
        if detections(i,2) - detections(i,1) > opts.theshlen
            candaidates = [ candaidates ; detections(i,:) ];
        end
    end
    fprintf('[%d] [candaidates %d]\n',seq_id1,size(candaidates,1));
    allcandidates{seq_id1} = candaidates;
    save(FILE,'allcandidates');    
end


end

function out = tempPool(data,M)    
    N = size(data,1);
    W = size(M,1);
    range = (W-1)/2:N-(W-1)/2-1;
    out = zeros(numel(range),size(data,2),'single');
    for i = 1 : numel(range)
        r = range(i)-((W-1)/2)+1:range(i)+((W-1)/2)+1;
        out(i,:) = sum(data(r,:).*M);
        %out(i,:) = genRepresentation(data(r,:),1,'ssr');
        if mod(i,80) == 0, fprintf('.'); end
        if mod(i,6400) == 0, fprintf('\n'); end
    end
    fprintf('\n');
end

function a = getChange(in)
    a = double(in(2:end)) - double(in(1:end-1));
end