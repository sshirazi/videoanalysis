function dataset = getCookingDataset(issareh,bowfeat)

if issareh 
    dataset.videodir = '';
    dataset.framedir = '';
    dataset.bowfeatdir  = sprintf('/data/home/basura/Dataset/cooking/feats/%s/cache/cvpr12/kmeans-k4000niter50redo8Down100000hard-0/IntegralHist',bowfeat);
    COOCKING_DATASET_PATH = '';
else
    dataset.videodir = '';
    dataset.framedir = '';
    COOCKING_DATASET_PATH = '/home/basura/Documents/Projects/actiondetectionV2/cooking_dataset/';
    dataset.bowfeatdir  = fullfile(sprintf('../Cooking_EXperiment_data/feats/%s/cache/cvpr12/kmeans-k4000niter50redo8Down100000hard-0/IntegralHist',bowfeat));
    
end

dataset.clipnames       = textread(fullfile(COOCKING_DATASET_PATH,'name.csv'),'%s');
dataset.clipannotations = csvread(fullfile(COOCKING_DATASET_PATH,'temporal.csv'));
dataset.videonames      = unique(dataset.clipnames);
dataset.dbpath          = COOCKING_DATASET_PATH;

for i = 1 : numel(dataset.videonames)
    a = strsplit(dataset.videonames{i},'-');
    dataset.matnames{i} = sprintf('%s-%s-hard-0',a{1},a{2});
end

end