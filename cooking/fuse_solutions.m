function [pr,rc,F] = fuse_solutions(varargin)
    opts.feat       = 'hof';
    opts.issareh    = false;
    opts.islite     = false;
    opts.id         = 0;
    opts.len        = 200;
    opts.detections = 50 ; 
    opts.isexecute  = true;      
    opts.resname    = 'hof&hof-w91&w61-s10-none-rank-none-len10&10-L2norm';
    opts = bc_argparse(opts,varargin);
    opts.str = sprintf('len%d-dec%d',opts.len,opts.detections);
    if opts.isexecute
    try
        [pr,rc,F] =  main(opts);
    catch err
        pid = feature('getpid');
        fid = fopen(sprintf('../logs/fuse_solutions/%s-pid%d.log',opts.str,pid),'a');
        fprintf(fid,'ERROR\n');
        fprintf(fid,'%s\n',err.identifier);
        fprintf(fid,'%s\n',err.message);
        fprintf(fid,'file = %s \n function  = %s\n Line =  %d\n',char(err.stack(end).file),err.stack(end).name,err.stack(end).line);
        fclose(fid);
    end
    else
        mats = dir('../detections/fusion_results/*.mat');
        for m = 1 :  numel(mats)
            load(['../detections/fusion_results/' mats(m).name]);
            [splits] = strsplit(mats(m).name,'-');
            fprintf('%s \t %s %s \t %1.3f   \t %1.3f   \t %1.3f\n',splits{1},splits{2},splits{3},pr,rc,F);
        end
    end
        
end

function [pr,rc,F] = main(opts)
if opts.islite
    opts.videos = 5;
else
    opts.videos = inf;
end
dataset = getCookingDataset(opts.issareh,opts.feat); 

resfileName = sprintf('../detections/fusion_results/len%d-detection-%d-%s',opts.len,...
    opts.detections,opts.resname);

if exist(resfileName,'file') == 2
    load(resfileName);
    pr = mean(precision*100);
    rc = mean(recall*100);
    F = 200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall));
    fprintf('precision  = %1.4f\n',pr);
    fprintf('recall     = %1.4f\n',rc);
    fprintf('fscore     = %1.4f\n',F);
    save(resfileName,'precision','recall','pr','rc','F');
    return;
else
    if ~opts.isexecute 
        pr = -1;
        rc = -1;
        F = -1;
        return;
    end
end

[~,numfiles] = system(sprintf('ls ../logs/fuse_solutions/%s-*.log | wc -l',opts.str));
if str2double(numfiles) > 0
    pr = -1;
    rc = -1;
    F = -1;
    return;
end


pid = feature('getpid');

r1 = getResultsSets('../detections/alldetections/baseline3-hof-W91-S10-Prenone-Poolrank-Postnone-LEN10-NormL2.mat');
r2 = getResultsSets('../detections/alldetections/baseline3-hof-W61-S10-Prenone-Poolrank-Postnone-LEN10-NormL2.mat');


pair_id = 1;
NPairs = min(opts.videos,numel(dataset.matnames))  * (min(opts.videos,numel(dataset.matnames))  -1) / 2;
recall = zeros(NPairs,1);
precision = zeros(NPairs,1);
detects = cell(NPairs,1);
for seq_id1 = 1 : min(opts.videos,numel(dataset.matnames))        
    gt1 = getCookingGTLablesForVIdeo(dataset,seq_id1);
    n1 = getCookingSequenceSize(opts.issareh,seq_id1);
    for seq_id2 =  seq_id1+1: min(opts.videos,numel(dataset.matnames))         
        gt2 = getCookingGTLablesForVIdeo(dataset,seq_id2);
        n2 = getCookingSequenceSize(opts.issareh,seq_id2);
        A = zeros(n1,n2,'single');        
        A = updateA(A,r1.detects{pair_id});
        A = updateA(A,r2.detects{pair_id});        
        detection = findDetectionsWithMinLen(A,opts);        
        if ~isempty(detection)
            [vals,scoreindx] = sort(detection(:,4),'descend');
            pr1 = zeros(min(opts.detections,size(detection,1))  ,2); 
            pr2 = zeros(min(opts.detections,size(detection,1))  ,2); 
            for di = 1 : min(opts.detections,size(detection,1))            
                ri = detection(scoreindx(di),1);
                k  = detection(scoreindx(di),3);
                ci = detection(scoreindx(di),2);            
                pr1(di,1) = ri;
                pr1(di,2) = ri+k;
                pr2(di,1) = ci;
                pr2(di,2) = ci + k;
            end      
            [detections2,gtdetection] = eveluateTwoSequencePredictions(gt1,gt2,pr1,pr2);
            precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
            recall(pair_id,1) = size(detections2,1)/size(pr1,1);                    
            detects{pair_id} = detections2;
            else
            precision(pair_id,1) = 0;
            recall(pair_id,1) = 0;
            detects{pair_id} =[];
        end        
        fprintf('[%d %d][mean P = %1.4f][mean R = %1.4f]\n',seq_id1,seq_id2, ...
            mean(precision(1:pair_id))*100, mean(recall(1:pair_id))*100);
        if mod(pair_id,5) == 1
            fid = fopen(sprintf('../logs/fuse_solutions/%s-pid%d.log',opts.str,pid),'a');
            fprintf(fid,'[%s][%d %d][mean P = %1.4f][mean R = %1.4f]\n',...
                char(datetime('now','Format','[dd.MM.yy][hh:mm:ss]')),seq_id1,seq_id2, ...
                mean(precision(1:pair_id))*100, mean(recall(1:pair_id))*100);
            fclose(fid);
        end
        
        pair_id = pair_id + 1;        
    end   
end    

pr = mean(precision*100);
rc = mean(recall*100);
F = 2 * pr * rc/ (pr + rc);
fprintf('precision  = %1.4f\n',pr);
fprintf('recall     = %1.4f\n',rc);
fprintf('fscore     = %1.4f\n',F);
save(resfileName,'precision','recall','pr','rc','F');

end

function A = updateA(A,detection)
    detection = tempoalNonMaximaSuppression(detection);
    
    for i =1: size(detection,1)
        if detection(i,2) < size(A,1) && detection(i,4) < size(A,2)
        A(detection(i,1):detection(i,2),detection(i,3):detection(i,4)) = A(detection(i,1):detection(i,2),detection(i,3):detection(i,4)) + eye(numel(detection(i,1):detection(i,2))).*detection(i,5);
        else
            fprintf('ERR');
        end
    end
    %fprintf('Detections %d ',size(detection,1));
end

function detection = findDetectionsWithMinLen(J,opts)
        detection = [];        
        det_id = 1;
        for ri = 1 : size(J,1)-1            
            ri_col_indx = find(J(ri,:)>0);
            for col_indx = 1 : numel(ri_col_indx)
                found_it = false;
                k = 1;
                score = J(ri,ri_col_indx(col_indx));
                while ~found_it && (ri+k <= size(J,1) && ri_col_indx(col_indx)+k <= size(J,2))
                    if(J(ri+k,ri_col_indx(col_indx)+k)) == 0
                        found_it = true;
                        J(ri,ri_col_indx(col_indx)) = 0;
                    else
                        score = score + J(ri+k,ri_col_indx(col_indx)+k);
                        J(ri+k,ri_col_indx(col_indx)+k) = 0;
                    end
                    k = k + 1;
                end
                k = k -1;
                if k >= opts.len 
                    detection(det_id,1) = ri;
                    detection(det_id,2) = ri_col_indx(col_indx);
                    detection(det_id,3) = k;
                    detection(det_id,4) = score/k;                             
                    det_id = det_id + 1;
                end
            end
        end        
end

function r1 = getResultsSets(detectionFile)
    load(detectionFile);
    for i = 1 : numel(alldetections)
    r1.detects{i} = alldetections{i}.detections;
    end
end
