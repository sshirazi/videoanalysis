function nDimsX = getCookingSequenceSize(issareh,id)
    if nargin < 1, issareh = false; end
    dataset = getCookingDataset(issareh,'hof');        
    matObj = matfile(fullfile(dataset.bowfeatdir,sprintf('%s.mat',dataset.matnames{id})));
    info = whos(matObj,'integralFrameHist');
    nDimsX = info.size(2);    
end