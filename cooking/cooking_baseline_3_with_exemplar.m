
function cooking_baseline_3_with_exemplar()

main();
end

function liteAll()
windows = [ 31 61 91];
strides = [ 10 30 60 ];
detectionss = [50 100 200 500 1000];
poolmethods = {'apr','rank'};
lens = [2 5 10 ];
prepoolnonlinears = {'none','ssr','chi2exp'};
postpoolnonlinears = {'none','ssr','chi2exp'};
postpoolnorms = {'none','L2'};

greedyexpconfig = [];
if exist('greedy_Exemplar_expconfig.mat','file'),load('greedyexpconfig'); end
id = 1;
for window = windows
    for stride = strides
        for detections = detectionss
            for poolmethod = poolmethods
                for len = lens
                    for prepoolnonlinear = prepoolnonlinears
                        for postpoolnonlinear  =  postpoolnonlinears
                            for postpoolnorm = postpoolnorms
                                 if (id-1)~= size(greedyexpconfig,2)
                                    id = id + 1;
                                  else
    [pr,rc,F] = main('window',window,'stride',stride,'detections',detections,...
        'poolmethod',poolmethod{1},'len',len,'prepoolnonlinear',prepoolnonlinear{1},...
        'postpoolnonlinear',postpoolnonlinear{1},'postpoolnorm',postpoolnorm{1},'id',id);
    
                        greedyexpconfig(id).window = window;
                        greedyexpconfig(id).stride = stride;
                        greedyexpconfig(id).detections = detections;
                        greedyexpconfig(id).poolmethod = poolmethod{1};
                        greedyexpconfig(id).len = len;
                        greedyexpconfig(id).prepoolnonlinear = prepoolnonlinear{1};
                        greedyexpconfig(id).postpoolnonlinear = postpoolnonlinear{1};
                        greedyexpconfig(id).postpoolnorm = postpoolnorm{1};
                        
                        greedyexpconfig(id).recall = rc;
                        greedyexpconfig(id).precision = pr;
                        greedyexpconfig(id).fscore = F;
                        save('greedy_Exemplar_expconfig.mat','greedyexpconfig');
                        id = id + 1;
                                 end
    
                            end
                        end
                    end
                end
            end
        end
    end
end

end


function [pr,rc,F] = main(varargin)
opts.feat       = 'hof';
opts.issareh    = false;
opts.islite     = false;
opts.id         = 0;
if opts.islite
    opts.videos = 5;
else
    opts.videos = inf;
end

opts.window     = 61;
opts.stride     = 10;
opts.detections = 100;
opts.poolmethod = 'apr';
opts.len        = 2;

opts.prepoolnonlinear = 'none';
opts.postpoolnonlinear = 'none';
opts.postpoolnorm = 'L2';


opts = bc_argparse(opts,varargin);
issareh = opts.issareh;
WindowSize = opts.window;
Stride     = opts.stride;
NUM_DETECTIONS  = opts.detections; 
POOL_METHOD     = opts.poolmethod;
dataset = getCookingDataset(issareh,opts.feat);       
pair_id = 1;


results_file = sprintf('results-%s-W%d-S%d-ND%d-%s-%s-%s-Exemplar-greedy-LEN%d-Norm%s.mat',opts.feat,...
    WindowSize,Stride,NUM_DETECTIONS,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm);

if ~opts.islite
DATA_FILE = generateTemporalEncodesAndSave('feat',opts.feat,'issareh',...
    opts.issareh,'window',opts.window,'stride',opts.stride,'prepoolnonlinear',...
    opts.prepoolnonlinear,'poolmethod',opts.poolmethod);
end

if ~opts.islite
    matObj = matfile(DATA_FILE);
    VID = matObj.VID;
end
for seq_id1 = 1 : min(opts.videos,numel(dataset.matnames))        
    gt1 = getCookingGTLablesForVIdeo(dataset,seq_id1);
    
    if ~opts.islite
        rankpool1 = matObj.data(find(VID == seq_id1),:);
    else
         feats = loadCookingSequences(dataset,seq_id1);   
         [rankpool1] = getEncode(feats,'window',opts.window,'stride',opts.stride,...
                'prepoolnonlinear',opts.prepoolnonlinear,'poolmethod',opts.poolmethod);
    end
    
    nDimsX = getCookingSequenceSize(issareh,seq_id1);
    range1 = (WindowSize-1)/2+1 : Stride : nDimsX -(WindowSize-1)/2;
    rankpool1 = getNonLinearity(rankpool1,opts.postpoolnonlinear);
    
    for seq_id2 =  seq_id1+1: min(numel(dataset.matnames),opts.videos) 
        timest = tic();
        gt2 = getCookingGTLablesForVIdeo(dataset,seq_id2); 
        
         if ~opts.islite
             rankpool2 = matObj.data(find(VID == seq_id2),:);
         else
              feats = loadCookingSequences(dataset,seq_id2); 
              [rankpool2] = getEncode(feats,'window',opts.window,'stride',opts.stride,...
                'prepoolnonlinear',opts.prepoolnonlinear,'poolmethod',opts.poolmethod);
         end
        
        nDimsX = getCookingSequenceSize(issareh,seq_id2);  
        range2 = (WindowSize-1)/2+1 : Stride : nDimsX-(WindowSize-1)/2;
        clear nDimsX;               
        
        rankpool2 = getNonLinearity(rankpool2,opts.postpoolnonlinear);        
        if strcmp(opts.postpoolnorm,'L2')           
            rankpool2 = normalizeL2(rankpool2);
        end       
        
        A = rankpool1 *rankpool2';
        
        % hueristic greedy method
        M = A;
        meanA = mean(A(:));
        stdA = std(A(:));
        % Find high confident pairwise similarity
        M(find(M < (meanA + stdA))) = 0;     
        % Initialize the final matching matrix
        J = zeros(size(M));        
        % enumulate each element in the left bi-partite graph
        LEN = 2;
        for ri = 1 : size(M,1)-LEN       
            % find the connected components for the node ri 
            ri_col_indx = find(M(ri,:)>0);
            % initialize candidates
            possible = ri_col_indx;
            % Now iterate for minimum depth of LEN
            for k = 1 : LEN
                ri_next_col_indx = find(M(ri+k,:)>0);
                possible = intersect(possible,ri_next_col_indx-k);
            end
            for k = 1 : LEN
                J(ri+k-1,possible+k-1) = A(ri+k-1,possible+k-1);                
            end
            eliminate = ones(1,size(M,2));
            eliminate(possible) = 0;
            M(ri,find(eliminate==1)) = 0;           
        end               
        
        detection = [];        
        det_id = 1;
        for ri = 1 : size(J,1)-1            
            ri_col_indx = find(J(ri,:)>0);
            for col_indx = 1 : numel(ri_col_indx)
                found_it = false;
                k = 1;
                score = J(ri,ri_col_indx(col_indx));
                while ~found_it
                    if(J(ri+k,ri_col_indx(col_indx)+k)) == 0
                        found_it = true;
                        J(ri,ri_col_indx(col_indx)) = 0;
                    else
                        score = score + J(ri+k,ri_col_indx(col_indx)+k);
                        J(ri+k,ri_col_indx(col_indx)+k) = 0;
                    end
                    k = k + 1;
                end
                k = k -1;
                if k >= opts.len
                    detection(det_id,1) = ri;
                    detection(det_id,2) = ri_col_indx(col_indx);
                    detection(det_id,3) = k;
                    detection(det_id,4) = score/k;                             
                    det_id = det_id + 1;
                end
            end
        end        
        
        if ~isempty(detection)
        [scorevals,scoreindx] = sort(detection(:,4),'descend');
        pr1 = []; pr2 = [];
        for di = 1 : min(NUM_DETECTIONS,size(detection,1))            
            ri = detection(scoreindx(di),1);
            k  = detection(scoreindx(di),3);
            ci = detection(scoreindx(di),2);            
            pr1(di,1) = range1(ri) - (WindowSize-1)/2;
            pr1(di,2) = range1(ri+k) + (WindowSize-1)/2;
                
            pr2(di,1) = range2(ci) - (WindowSize-1)/2;
            pr2(di,2) = range2(ci+k) + (WindowSize-1)/2;
        end      
            [detections2,gtdetection] = eveluateTwoSequencePredictions(gt1,gt2,pr1,pr2);
            precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
            recall(pair_id,1) = size(detections2,1)/size(pr1,1);        
            timest = toc(timest);
            detects{pair_id} = detections2;
        else
            precision(pair_id,1) = 0;
            recall(pair_id,1) = 0;
            detects{pair_id} =[];
        end
        
        
     
        
        fprintf('[Greedy %d][W%d-S%d-D%d-%s-L%d-%s-%s-%s][%d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
            opts.id,opts.window,opts.stride,opts.detections,opts.poolmethod,opts.len,...
            opts.prepoolnonlinear,opts.postpoolnonlinear,opts.postpoolnorm,...
            seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
            mean(precision*100),mean(recall*100),timest);                            
        
        pair_id = pair_id + 1; 
        if ~opts.islite
            save(results_file,'precision','recall','pair_id','detects','seq_id1','seq_id2');  
        end
    end    
end
pr = mean(precision*100);
rc = mean(recall*100);
F = 200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall));

end

function DATA_FILE_EXE = generateTemporalEncodesAndSave(varargin)
    
    opts.feat               = 'hof';
    opts.issareh            = false;
    opts.window             = 61;
    opts.stride             = 50;
    opts.prepoolnonlinear   = 'ssr';
    opts.poolmethod         = 'rank';
    opts = bc_argparse(opts,varargin);
    
    issareh = opts.issareh;
    WindowSize = opts.window;
    Stride     = opts.stride;

    dataset = getCookingDataset(issareh,opts.feat);
    DATA_FILE = sprintf('../Cooking_CVPR17_Seq_Data/Feat-%s-Window%d-Stride%d-Pool%s-Rank%s.mat',...
        opts.feat,WindowSize,Stride,opts.prepoolnonlinear,opts.poolmethod);

    switch opts.prepoolnonlinear
        case {'none','ssr'}
            dim = 2;
        case 'chi2exp'
            dim = 12;   
        case 'chi1'
            dim = 6;       
        case 'chi2'
            dim = 10;           
    end
    if exist(DATA_FILE,'file') ~= 2
        count = 0;
        for seq_id1 = 1 : numel(dataset.matnames)
            nDimsX = getCookingSequenceSize(issareh,seq_id1);    
            count = count + numel((WindowSize-1)/2+1 : Stride : nDimsX-(WindowSize-1)/2);   
            fprintf('[%d ] [counting elements %d ]\n',seq_id1,count);
        end
        data = zeros(count,4000*dim,'single');
        VID = zeros(count,1,'single');
        st = 1;
        for seq_id1 = 1 : numel(dataset.matnames)
            timest = tic();
            feats = loadCookingSequences(dataset,seq_id1);   
            range = (WindowSize-1)/2+1 : Stride : size(feats,1)-(WindowSize-1)/2;
            rankpool = getEncode(feats,'window',opts.window,'stride',opts.stride,...
                'prepoolnonlinear',opts.prepoolnonlinear,'poolmethod',opts.poolmethod);
            data(st:st+numel(range)-1,:) = rankpool;
            VID(st:st+numel(range)-1,:) = seq_id1;
            st = st + numel(range);
            timest = toc(timest);
            fprintf('[%d ] [Temp pool %d ] [ Fps %1.1f] [Time %1.1f]\n',seq_id1,size(feats,1),size(feats,1)/timest,timest);    
        end
        save(DATA_FILE,'data','VID','-v7.3'); 
    else
        fprintf('Loading the data file %s \n',DATA_FILE);
        load(DATA_FILE);
        fprintf('*** done ****\n');
    end     
    
    % Applying exemplar SVM over temporal pooled features
     DATA_FILE_EXE = sprintf('../Cooking_CVPR17_Seq_Data/Feat-%s-Window%d-Stride%d-Pool%s-Rank%s-EXEMPLAR.mat',...
        opts.feat,WindowSize,Stride,opts.prepoolnonlinear,opts.poolmethod);

    if exist(DATA_FILE_EXE,'file')~=2
        st = 1;
        data2 = zeros(size(data),'single');
        data = data';
        nALL = size(data,2);
        for seq_id1 = 1 : numel(dataset.matnames)
            timest = tic();
            nDimsX = getCookingSequenceSize(issareh,seq_id1);  
            range = (WindowSize-1)/2+1 : Stride : nDimsX-(WindowSize-1)/2;            
            %rankpool = data(st:st+numel(range)-1,:);            
            seqRange = st:st+numel(range)-1;
            otherIndx = zeros(1,nALL);
            otherIndx(seqRange) = 1;
            otherIndx = find(otherIndx==0);
            rn = randperm(numel(otherIndx));
            otherIndx = otherIndx(rn(1:5000));
            otherData = data(:,otherIndx);
            Ws = cell(1,numel(seqRange));
            parfor eId=1:numel(seqRange)               
                trnData = [data(:,seqRange(eId)) otherData];
                training_label_vector = [1 zeros(1,numel(otherIndx))-1];                
                training_weight_vector = [1 ones(1,numel(otherIndx)).*(1/numel(otherIndx))];   
                [w, b] = vl_svmtrain(trnData, training_label_vector, 0.1,'weights',training_weight_vector) ;                
                Ws{eId} = w;
                if mod(eId,80) == 0
                    fprintf('\n')
                else
                    fprintf('.')
                end
            end    
            for eId=1:numel(seqRange)    
                data2(seqRange(eId),:) = Ws{eId};
            end
            fprintf('.\n')
            st = st + numel(range);
            timest = toc(timest);
            fprintf('[%d ] [Exemplar %d ] [ Fps %1.1f] [Time %1.1f]\n',seq_id1,nDimsX,nDimsX/timest,timest);    
        end    
        data = data2;
        save(DATA_FILE_EXE,'data','VID','-v7.3'); 
    end
end

function [rankpool] = getEncode(feats,varargin)

opts.window  = 61;
opts.stride  = 50;
opts.prepoolnonlinear = 'ssr';
opts.poolmethod = 'rank';
opts = bc_argparse(opts,varargin);

W = opts.window;
S = opts.stride;

range = (W-1)/2+1 : S : size(feats,1)-(W-1)/2;
nFrm = numel(range);
switch opts.prepoolnonlinear
    case {'none','ssr'}
        dim = 2;
    case 'chi2exp'
        dim = 12;   
    case 'chi1'
        dim = 6;       
    case 'chi2'
        dim = 10;      
     case 'ser'
        dim = 4;       
end
rankpool = zeros(nFrm,size(feats,2)*dim,'single');
for p =1 : numel(range)
   i = range(p); 
   if i == range(end) 
       data = feats(i - (W-1)/2 : max(i+(W-1)/2,size(feats,1)),:);   
   else
       data = feats(i - (W-1)/2 : i+(W-1)/2,:);   
   end
   switch opts.poolmethod
       case 'rank'
           rankpool(p,:) = genRepresentation(data,1,opts.prepoolnonlinear);
       case 'apr'
           rankpool(p,:) = genAprRepresentation(data,1,opts.prepoolnonlinear);
   end
end

end

