%clear
%clc

%% Read data
function [meanpurity,meancoverage] =  baseline2_clustering_v2()
codePath = '/home/sareh/Codes/Action_prediction_stuff/IKEA_Codes';
Path = '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/cooking_data/';
dataPath = '/home/sareh/Datasets/cooking_sample/';
info = dir(dataPath);
isub = [info(:).isdir];  %% List of the folders
runs = {info(isub).name}';
runs(ismember(runs,{'.','..'})) = [];  %% Removing '.' and '..'

window = 20;
pooling = 'max'; % 'max','mean','rank'
add_time = 'on';
ClusMethod = 'kmeans';
%% Read the deep features
fileX_saved = 'yes';

for iter = 2:2%length(runs)
    if strcmp(fileX_saved,'no')
        % Read the frames in each run
        runPath_im = [dataPath runs{iter}];
        info = dir(runPath_im);
        frames = {info.name};
        frames(ismember(frames,{'.DS_Store','.','..'})) = [];

        runPath = [Path 'deep/cooking_' runs{iter}];
        info = dir(runPath);
        images = {info.name};
        images(ismember(images,{'.DS_Store','.','..'})) = [];
        X = zeros(length(images),2048,'single');
        for iter2 = 1:length(images)
            im = imread([runPath_im '/' frames{iter2}]);
            info = strsplit(images{iter2},'_');
            hinfo = hdf5info([runPath '/' images{iter2}]);
            dset1 = hdf5read(hinfo.GroupHierarchy.Datasets(1));
    %         if rem(iter2,window) ~= 0
    %             index = floor(iter2/(window)) + 1;
    %         else
    %             index = floor(iter2/(window));
    %         end
    %         id = iter2 - ((index-1)*window);
            feature  = reshape(dset1,1,[]);
            X(iter2,:) = feature;
            fprintf('%d/%d \n',iter2, length(images));
        end
        file_video = [Path 'deep/cooking_' runs{iter}];
        save(file_video, 'X');
    elseif strcmp(fileX_saved,'yes')
        X = [];
        Z = [];
        runPath = [Path 'deep/cooking_' runs{iter} '.mat'];
        load(runPath);
    end
    [a,b] = size(X);
    Z = zeros(a,b+1);
    Z(:,1:b) = X;
    % Add the time stamp to the end of the features
    if (strcmp(add_time,'on'))
        X = [ X (1:a)'];
        Y = zeros(size(X,1)-window+1,2049);
    else
        Y = zeros(size(X,1)-window+1,2048);
    end
    
    if  strcmp(pooling,'max')
        for iter3 = 1:size(X,1)-window+1
            Y(iter3,:) = max(X(iter3:iter3+window-1,:));
            fprintf('%d/%d \n',iter3, size(X,1)-window+1);
        end
    elseif strcmp(pooling,'mean')
        for iter3 = 1:size(X,1)-window+1
            Y(iter3,:) = mean(X(iter3:iter3+window-1,:));
            fprintf('%d/%d \n',iter3, size(X,1)-window+1);
        end
    elseif strcmp(pooling,'rank')
        %TODO
    end
end

%% Clustering the data - kmeans

Num_Clusters = 20;
Y(:,1:end-1) = normalizeL2(Y(:,1:end-1));

switch ClusMethod
    case 'kmeans'
        [~, idx] = vl_kmeans(Y', Num_Clusters, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 100) ;
    case 'som'
        net = selforgmap([4 5]);
        net.trainParam.showWindow = false;
        net.trainParam.showCommandLine = true;
        net = train(net,Y');
        y = net(Y');
        idx = vec2ind(y);
end
%[idx,C,sumd,D] = kmeans(Y,Num_Clusters);
[meanpurity,meancoverage] = getClusterPurity(idx);

end
    
%% Matching features

% videos = [2,5];
% maximum = 0;
% tmp = 1;
% for p1=1:size(Y{videos(1)},1)
%     for q1=p1+1:size(Y{videos(1)},1)
%         tmp = 1;
%         for p2=1:size(Y{videos(2)},1)
%             for q2=p2+1:size(Y{videos(2)},1)
%                 temp = tmp * max(0,Y{videos(1)}(p1,:)*Y{videos(2)}(p2,:)');
%                 if(tmp > maximum)
%                     maximum = tmp;
%                     start = p2;
%                     finish = q2;
%                 end
%             end
%         end
%         
%     end
% end
% 

