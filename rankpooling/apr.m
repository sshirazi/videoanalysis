function w = apr(Data,C,normD)
    if normD == 2
        Data = normalizeL2(Data);
    end    
    if normD == 1
        Data = normalizeL1(Data);
    end    
    T = size(Data,1);
    t = 1 : T;
    at = single(2 * ( T - t +1) - (T + 1)*(myHarmonic(T) - myHarmonic(t-1)));  
    w = sum(repmat(at',1,size(Data,2)) .* Data);
    w = w';
end

function   a = myHarmonic(T)
    if numel(T) == 1
        if T==0
            a = 0;
        else
            a = sum(1./(1:T));
        end
    else
        a = zeros(size(T));
        for i = 1 : numel(T)
           if T(i) == 0
                a(i) = 0;
           else
                a(i) = sum(1./(1:T(i)));
            end 
        end
    end
end