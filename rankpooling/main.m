
% *************************************************************************
% Dependency : Lib Linear 
% Author : Basura Fernando

CVAL    = 1;    % The defaul C value for SVR
WIN     = 20;   % The window size of the poooling layers
STRIDE  = 1;    % The stride of the pooling layer
% define the network architecture
net = defineNetwork(CVAL,WIN,STRIDE);   

% create a sequence of 100 vectors of dimensionality 1024
sequence_data = randn(100,1024);
% get the encoding of the sequence
W = passNetwork(sequence_data,net)  ;