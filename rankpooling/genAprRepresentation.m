% Rank pooling main function
%
% Author: Basura Fernando
function W = genAprRepresentation(data,at,nonLin)    
    W_fow = apr(getNonLinearity(data,nonLin),at,2); clear Data; 			
    order = 1:size(data,1);
    [~,order] = sort(order,'descend');
    data = data(order,:);    
    W_rev = apr(getNonLinearity(data,nonLin),at,2); 			              
    W = [W_fow ; W_rev]; 
end