function SalientFeatExtract(issareh)
    if nargin < 1
        issareh = false;
    end    
    params.numberofboxes = 100;
    params.windowsize    = 11;

    if issareh
        MATCOV_PATH =  '/home/sareh/Codes/matconvnet/matlab';
        RESNET_PATH =  '/home/sareh/Codes/matconvnet/models/imagenet-resnet-152-dag.mat';
        MAIN_DATA = '/home/sareh/Datasets/IKEA/data/'; 
        OUT_DIR = '/home/sareh/Datasets/IKEA/SalinteFeats';
        EDGE_BOXES_MODEL = '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/edges/models/forest/modelBsds.mat';
        EDGE_PATH       = '';
        EDGE_DEPENDECY_PATH ='';
        warning('set EDGE_PATH');        
    else
        MATCOV_PATH     = '~/lib/matconvnet/matlab';
        RESNET_PATH     =  '/data/home/basura/matcov_cnn_models/imagenet-resnet-152-dag.mat';
        MAIN_DATA       = '/data/home/basura/Dataset/IKEA/data'; 
        MAIN_DATA       = '/data/home/basura/Dataset/cooking/frame';
        OUT_DIR         = '/data/home/basura/Dataset/cooking/features/SalinteFeats-resnet-152';
        OUT_DIR         = '/data/home/basura/Dataset/IKEA/features/SalinteFeats-resnet-152';
        EDGE_BOXES_MODEL= '/home/basura/lib/edges/models/forest/modelBsds.mat';
        EDGE_PATH       = '/home/basura/lib/edges';
        EDGE_DEPENDECY_PATH = '/home/basura/lib/toolbox';
    end
    addpath(EDGE_PATH); addpath(genpath(EDGE_DEPENDECY_PATH));
    addpath(MATCOV_PATH);
    vl_setupnn();
    
    %net = dagnn.DagNN.loadobj(load(RESNET_PATH)) ;
    %net.mode = 'test' ;
    %net.conserveMemory = false;
    
   
   runs = dir(MAIN_DATA);    
   model = load(EDGE_BOXES_MODEL);
    
   Windowsize = params.windowsize;
   T = Windowsize;  
   t = 1 : T;
   at = single(2 * ( T - t +1) - (T + 1)*(harmonic(T) - harmonic(t-1)));      
   global maxv 
   maxv = max(at);
   global minv 
   minv = min(at);
   figure,
    for run = 24 : numel(runs)        
        imgs = dir(sprintf('%s/*.jpg',fullfile(MAIN_DATA,runs(run).name)));
        im = single(imread(fullfile(MAIN_DATA,runs(run).name,imgs(1).name)));                     
        file = fullfile(OUT_DIR,sprintf('%s.mat',runs(run).name));
        if exist(file,'file') == 2
            continue;
        end
        range = (Windowsize-1)/2+1 : numel(imgs)-(Windowsize-1)/2;
        feats = zeros(numel(range),2048,'single');
        feats2 = zeros(numel(range),2048,'single');          
        for i = range            
            IMG = zeros(size(im),'single');
            counter = 1;
            for p = i - (Windowsize-1)/2 : i+(Windowsize-1)/2
                im = single(imread(fullfile(MAIN_DATA,runs(run).name,imgs(p).name)));                     
                IMG = IMG + im .*  at(counter);
                counter = counter  + 1;
            end
            IMG = getBackRGBImage(IMG(:,:,1),IMG(:,:,2),IMG(:,:,3));
            
            bbs = edgeBoxes( IMG, model.model,'maxBoxes', params.numberofboxes);
            
            if size(bbs,1) > 0
                bbmean = mean(bbs(:,5));
                x_min = min(bbs(:,1));
                y_min = min(bbs(:,2));
                x_max = max(bbs(:,1)+bbs(:,3));
                y_max = max(bbs(:,2)+bbs(:,4));   
                width = x_max - x_min;
                height = y_max - y_min;
            else
                bbmean = 0;
                x_min = 1;
                y_min = 1;
                width  = size(im,2);
                height  = size(im,1);
            end

            imshow(IMG),title(sprintf('%1.2f',bbmean));
            hold on           
%             for j = 1 : size(bbs,1)
%                 rectangle('Position',[bbs(j,1), bbs(j,2), bbs(j,3), bbs(j,4)],'EdgeColor','r');
%             end
            rectangle('Position',[x_min,y_min,width,height],'EdgeColor','g');
            hold off
            drawnow;
            pause(0.1);

             
%            crop_im = IMG(y_min:y_max,x_min:x_max);            
%             im = imread(fullfile(MAIN_DATA,runs(run).name,imgs(i).name));           
%             im = im(y_min:y_max,x_min:x_max);                        
%             f1 = getFeat(im,net);            
%             f2 = getFeat(crop_im,net); 
%             feats(i,:) = f1;
%             feats2(i,:) = f2;
            fprintf('run %s img %d of %d == %1.2f\n',runs(run).name,i,numel(imgs),bbmean);
        end
        save(file,'feats','feats2');
    end
end

function f = getFeat(crop_im,net)
    crop_im = imresize(crop_im, net.meta.normalization.imageSize(1:2)) ;
    crop_im = bsxfun(@minus, single(crop_im), net.meta.normalization.averageImage) ;                
    net.eval({'data', crop_im}) ;
    f = squeeze(net.vars(net.getVarIndex('pool5')).value) ;
end


function x = linearMapping(x)
    %minV = min(x(:));
    %maxV = max(x(:));
   global maxv;    
   global minv 
    minV = minv*256;
    maxV = maxv*256;
    x = x - minV;
    x = x ./ (maxV - minV);
    x = x .* 255;
    x = uint8(x);
end

function z  = getBackRGBImage(r,g,b)
    z(:,:,1) = linearMapping(r);
    z(:,:,2) = linearMapping(g);
    z(:,:,3) = linearMapping(b);
    z = uint8(z);
end