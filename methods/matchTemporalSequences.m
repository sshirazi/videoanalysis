function [video_1_detection,video_2_detection] = matchTemporalSequences(rankpool1,rankpool2,max_detections,WindowSize,range1,range2)

% get similarity between sub-sequences
A = normalizeL2(getNonLinearity(rankpool1,'none'))*normalizeL2(getNonLinearity(rankpool2,'none'))';

max_detections = min(max_detections,numel(find(A>0.2)));
video_1_detection = zeros(max_detections,2);
video_2_detection = zeros(max_detections,2);
for detection_id = 1 : max_detections
    [~,I] = max(A(:));
    [I_row, I_col] = ind2sub(size(A),I);
    % get detection for video 1
    i = range1(I_row);
    video_1_detection(detection_id,1) = i - (WindowSize-1)/2 ;
    video_1_detection(detection_id,2) = i + (WindowSize-1)/2 ;    
    % get detection for video 2
    i = range2(I_col);
    video_2_detection(detection_id,1) = i - (WindowSize-1)/2 ;
    video_2_detection(detection_id,2) = i + (WindowSize-1)/2 ;
    A(I_row,I_col) = -inf;
end
    

end


