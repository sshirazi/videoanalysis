%
% This method first apply temporal pooling on the features.
% Then it construct the similarity.
%
% First kmeans the features and then temporal encode.
% Then it construct the cluster similaity.
function compareSequences(feats1,feats2)
feats1L2 = normalizeL2(feats1);
feats2L2 = normalizeL2(feats2);
window = 50;

[maxpool1,meanpool1,medianpool1,rankpool1] = getTempEncode(feats1,window);
[maxpool2,meanpool2,medianpool2,rankpool2] = getTempEncode(feats2,window);

maxpool1L2 = normalizeL2(maxpool1);
maxpool2L2 = normalizeL2(maxpool2);
meanpool1L1 = normalizeL2(meanpool1);
meanpool2L2 = normalizeL2(meanpool2);
medianpool1L1 = normalizeL2(medianpool1);
medianpool1L2 = normalizeL2(medianpool2);
rankpool1L2 = normalizeL2(rankpool1);
rankpool2L2 = normalizeL2(rankpool2);

[M] = getClusterEncoding(feats1,feats2,'rank');
figure,imagesc(M), title('cluster enc with rank');

[M] = getClusterEncoding(feats1,feats2,'rank');
figure,imagesc(M), title('cluster enc with rank');

M = feats1L2 * feats2L2';
figure,hold on, 
subplot(3,2,1),imagesc(M),title('feats');
M = maxpool1L2 * maxpool2L2';
subplot(3,2,3), imagesc(M),title('max pool');

M = meanpool1L1 * meanpool2L2';
subplot(3,2,4), imagesc(M),title('mean pool');

M = medianpool1L1 * medianpool1L2';
subplot(3,2,5), imagesc(M),title('median pool');

M = rankpool1L2 * rankpool2L2';
subplot(3,2,6), imagesc(M),title('rank pool');

hold off;



end

function [maxpool,meanpool,medianpool,rankpool] = getTempEncode(feats,W)
maxpool = zeros(size(feats,1) - W + 1,size(feats,2),'single');
meanpool = maxpool;
medianpool = meanpool;
rankpool = zeros(size(feats,1) - W + 1,size(feats,2)*2,'single');
for i = 1 : size(feats,1) - W + 1 
   data = feats(i:i+W-1,:);
   maxpool(i,:) = max(data);
   meanpool(i,:) = mean(data);
   medianpool(i,:) = median(data);
   rankpool(i,:) = genRepresentation(data,1,'ssr');
end
    

end

function [M] = getClusterEncoding(maxpool1L2,maxpool2L2,enc)

maxpool1L2 = [ maxpool1L2 (1:size(maxpool1L2,1))'];
maxpool2L2 = [ maxpool2L2 (1:size(maxpool2L2,1))'];

Num_Clusters = 20;
[~, idx1] = vl_kmeans(maxpool1L2', Num_Clusters, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 100) ;
[~, idx2] = vl_kmeans(maxpool2L2', Num_Clusters, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 100) ;


 switch enc
        case 'max'
            out1 = zeros(Num_Clusters,size(maxpool1L2,2),'single');
            out2 = zeros(Num_Clusters,size(maxpool1L2,2),'single');
        case 'rank'
            out1 = zeros(Num_Clusters,size(maxpool1L2,2)*2,'single');
            out2 = zeros(Num_Clusters,size(maxpool1L2,2)*2,'single');    
end
for i = 1:Num_Clusters
    data1 = maxpool1L2(find(idx1 == i),:);
    data2 = maxpool2L2(find(idx2 == i),:);
    switch enc
        case 'max'
            out1(i,:) = max(data1);
            out2(i,:) = max(data2);
         case 'rank'
            out1(i,:) = genRepresentation(data1,1,'ssr');
            out2(i,:) = genRepresentation(data2,1,'ssr');    
    end
end

out1 = normalizeL2(out1);
out2 = normalizeL2(out2);

M = out1*out2';

end