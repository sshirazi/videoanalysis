function [maxpool,meanpool,medianpool,rankpool,range] = getTemporalEncode(feats,W,S)
range = (W-1)/2+1 : S : size(feats,1)-(W-1)/2;
nFrm = numel(range);
%maxpool = zeros(nFrm,size(feats,2),'single');
%meanpool = maxpool;
%medianpool = meanpool;
maxpool = [];
meanpool = [];
medianpool = [];
rankpool = zeros(nFrm,size(feats,2)*12,'single');
for p =1 : numel(range)
   i = range(p); 
   if i == range(end) 
       data = feats(i - (W-1)/2 : max(i+(W-1)/2,size(feats,1)),:);   
   else
       data = feats(i - (W-1)/2 : i+(W-1)/2,:);   
   end
%  maxpool(index,:) = max(data);
%  meanpool(index,:) = mean(data);
%  medianpool(index,:) = median(data);
  rankpool(p,:) = genRepresentation(data,1,'chi2exp');
%   rankpool(p,:) = genAprRepresentation(data,1,'chi2exp');   
end
end