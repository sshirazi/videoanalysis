function [correct,totalGt] = eveluateTwoSequencePredictions(gt1,gt2,pr1,pr2)
threshold = 0.2;
CID = unique([gt1(:,3) ; gt2(:,3)]);
CID(find(CID == 1)) = [];
correct = [];
totalGt = [];
for cl = 1 : numel(CID)
    if numel(find(gt1(:,3) == CID(cl) )) > 0 && numel(find(gt2(:,3) == CID(cl) )) > 0
        clip_indxs1 = find(gt1(:,3) == CID(cl));
        clip_indxs2 = find(gt2(:,3) == CID(cl));   
        set1 = gt1(clip_indxs1,:);
        set2 = gt2(clip_indxs2,:);
        for match_id = 1: size(pr1,1)                                
            for c1 = 1 : numel(clip_indxs1)
                if ~isValid(pr1(match_id,:),set1(c1,:),threshold), continue, end
                for c2 = 1 : numel(clip_indxs2)              
                    if ~isValid(pr2(match_id,:),set2(c2,:),threshold), continue, end                        
                    correct = [correct ; [set1(c1,:) set2(c2,:) pr1(match_id,:) pr2(match_id,:)] ];                         
                end
            end            
        end        
    end    
end

% 
% for c1 = 1 : numel(clip_indxs1)
%             for c2 = 1 : numel(clip_indxs2)                
%                 for match_id = 1: size(pr1,1)                    
%                     valid = isCorrectSegement(set1(c1,:),set2(c2,:),pr1(match_id,:),pr2(match_id,:),threshold);
%                     if valid
%                         correct = [correct ; [set1(c1,:) set2(c2,:) pr1(match_id,:) pr2(match_id,:)] ]; 
%                     end
%                 end
%             end
%         end        


for cl = 1 : numel(CID)
    if numel(find(gt1(:,3) == CID(cl) )) > 0 && numel(find(gt2(:,3) == CID(cl) )) > 0
        clip_indxs1 = find(gt1(:,3) == CID(cl));
        clip_indxs2 = find(gt2(:,3) == CID(cl));   
        set1 = gt1(clip_indxs1,:);
        set2 = gt2(clip_indxs2,:);
        for c1 = 1 : numel(clip_indxs1)
            for c2 = 1 : numel(clip_indxs2)
                totalGt = [totalGt ; set1(c1,:) set2(c2,:)];                
            end
        end        
    end    
end


end

function valid = isCorrectSegement(gt1,gt2,pr1,pr2,threshold)
    iou1 = numel(intersect(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)))/numel(union(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)));
    if iou1 < threshold, valid = false; return; end
    iou2 = numel(intersect(pr2(1,1):pr2(1,2),gt2(1,1):gt2(1,2)))/numel(union(pr2(1,1):pr2(1,2),gt2(1,1):gt2(1,2)));
    if iou2 < threshold, valid = false; return; end
    valid = true;
end

function valid = isValid(pr1,gt1,threshold)
    iou1 = numel(intersect(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)))/numel(union(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)));
    if iou1 < threshold, valid = false; else, valid = true;   end
end

