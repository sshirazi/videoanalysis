%
% gt1 : ground truth for vodeo 1 gt1 = [3 * N] matrix [t1 t2 class];
% gt2 : ground truth for video 2 gt2 = [3 * M] matrix [t1 t2 class];
% pr1, pr2 predictions for video 1 and 2. where pr1 = [2 * K] maatrix
% dropClasses : classes that should not be considered, example the
% background class 
% threshold = UIO threhold default = 0.5

function [correct,totalGt] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,dropClasses,threshold)
if nargin < 5
    dropClasses = [];
end
if nargin < 6
    threshold = 0.5;
end
CID = unique([gt1(:,3) ; gt2(:,3)]);
for dc = dropClasses
    CID(find(CID == dc)) = [];
end
correct = [];
totalGt = [];
gtClass  = [];
for cl = 1 : numel(CID)
    if numel(find(gt1(:,3) == CID(cl) )) > 0 && numel(find(gt2(:,3) == CID(cl) )) > 0
        clip_indxs1 = find(gt1(:,3) == CID(cl));
        clip_indxs2 = find(gt2(:,3) == CID(cl));   
        set1 = gt1(clip_indxs1,:);
        set2 = gt2(clip_indxs2,:);
        for c1 = 1 : numel(clip_indxs1)
            for c2 = 1 : numel(clip_indxs2)
                totalGt = [totalGt ; set1(c1,:) set2(c2,:)];    
                gtClass = [gtClass  cl];
            end
        end        
    end    
end



 for match_id = 1: numel(gtClass) 
      found = [];score = [];
      for d = 1 : size(pr1,1)
        [isvalid1,iou1] = isValid(pr1(d,:),totalGt(match_id,1:2),threshold);
        if ~isvalid1, continue, end        
        [isvalid2,iou2] = isValid(pr2(d,:),totalGt(match_id,4:5),threshold);
        if ~isvalid2, continue, end      
        score = [score ; (iou1 * iou2)];
        found = [found ; totalGt(match_id,:) pr1(d,:) pr2(d,:)];
      end
      if ~isempty(found)
            [~,best] = max(score);
            correct = [correct ;  found(best,:)]; 
      end
 end            



   





end

function valid = isCorrectSegement(gt1,gt2,pr1,pr2,threshold)
    iou1 = numel(intersect(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)))/numel(union(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)));
    if iou1 < threshold, valid = false; return; end
    iou2 = numel(intersect(pr2(1,1):pr2(1,2),gt2(1,1):gt2(1,2)))/numel(union(pr2(1,1):pr2(1,2),gt2(1,1):gt2(1,2)));
    if iou2 < threshold, valid = false; return; end
    valid = true;
end

function [valid,iou1] = isValid(pr1,gt1,threshold)
    iou1 = numel(intersect(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)))/numel(union(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)));
    if iou1 < threshold, valid = false; else, valid = true;   end
end

