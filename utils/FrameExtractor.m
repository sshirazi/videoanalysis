function list = FrameExtractor(fileName,out)
    bmkdir(out);
    [dirName,actionVideoName,extName]=fileparts(fileName); 
    frameFolder = sprintf('%s/%s',out,actionVideoName);
    if exist(frameFolder,'dir')~=7
        mkdir(frameFolder);
    end
    system(sprintf('rm -f -r %s/*',frameFolder));
    fP = sprintf('ffmpeg -i "%s" -f image2  "%s/',fileName,frameFolder);
    command = strcat(fP,'output_%05d.jpg"');
    %command
    system(command);    
    list = dir(sprintf('%s/*.jpg',frameFolder));    
end