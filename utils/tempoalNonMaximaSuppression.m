function detection = tempoalNonMaximaSuppression(detection)
    keep = [];
    remove = [];
    for i = 1 : size(detection,1)
        if numel(find(remove == i)) > 0 || numel(find(keep == i)) > 0
            continue;
        end
        dt1 = detection(i,:);
        isOverlapping = zeros(1,numel(i+1 : size(detection,1)));
        isOverlapping = (isOverlapping ~= 0);
        k = 1;        
        TTL = 2;
        while TTL > 0 && (i+k) <= size(detection,1)
            dt2 = detection(i+k,:);
            isOverlapping(k) = isOverlappingDetection(dt1,dt2);     
            if isOverlapping(k)
                TTL = TTL + 1;
            else
                TTL = TTL - 1;
            end            
            k = k + 1;
        end
        indx = find(isOverlapping>0);
        % if this detection is unique
        if isempty(indx)
            keep = [keep ; i ];
        else 
            indx = [(indx)+i i];
            [~,bestOne] =  max(detection(indx,5));            
            keep = [keep ; indx(bestOne) ];
            indx(bestOne) = [];
            remove = [remove indx] ; 
        end
        %if mod(i,10)==1, fprintf('.'); end
        %if mod(i,1000)==1, fprintf('\n'); end
    end
    keep = unique(keep);
    detection = detection(keep,:);
end

function isOverlapping = isOverlappingDetection(dt1,dt2)
    threshold = 0.5;
    isOverlapping = isValid(dt1(1,1:2),dt2(1,1:2),threshold) && isValid(dt1(1,3:4),dt2(1,3:4),threshold);
end


function valid = isValid(pr1,gt1,threshold)
    iou1 = numel(intersect(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)))/numel(union(pr1(1,1):pr1(1,2),gt1(1,1):gt1(1,2)));
    if iou1 < threshold, valid = false; else, valid = true;   end
end