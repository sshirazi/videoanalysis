
%
% This baseline just apply rank pooling and do the matching without any
% temporal consistency search
%
%
function ikea_baseline_2(varargin)
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/code_public';
addpath '/home/sareh/Codes/liblinear/matlab'
%addpath '/home/sareh/Codes/libsvm/matlab'
addpath '/home/sareh/Codes/Action_prediction_stuff/vlfeat/toolbox';
vl_setup();
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/utils';
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/methods';

opts.feats = {'hog','hof'};
opts.issareh = true;
opts.window  = 41;
opts.stride  = 20;
opts.poolmethod = 'rank';
opts.detections = 200;
opts.threshold = 0.2;
opts.islite = false;
opts = bc_argparse(opts,varargin);

issareh = opts.issareh;
WindowSize = opts.window;
Stride     = opts.stride;
NUM_DETECTIONS  = opts.detections; 
POOL_METHOD     = opts.poolmethod;

if opts.islite
    opts.videos = 5;
else
    opts.videos = inf;
end

dataset = [];
opts.featstr = [];
for feat = opts.feats
    dataset{end+1} = getIkeaDataset(opts.issareh,feat{1});       
    opts.featstr = [opts.featstr feat{1} '-'];
end
pair_id = 1;

results_file = sprintf('/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/Ikea/detections/ealyfusion/baseline2_fusion-%s-W%d-S%d-ND%d-greedy-Norm%s.mat',opts.featstr,...
    WindowSize,Stride,NUM_DETECTIONS,POOL_METHOD);
if exist(results_file,'file')
    sprintf('\n\n\n****Result file already exist! Please check before running!****\n\n\n')
    error('******Result file already exist! Please check before running*****');
end

% if ~opts.islite
%     DATA_FILE = [];
%     for feat = opts.feats
%         DATA_FILE{end+1} = load(sprintf('seq_data/Feat-%s-Window%d-Stride%d.mat',feat,opts.window,opts.stride));
%     end
% end

if ~opts.islite
    for i = 1 : numel(opts.feats)
        DATA_FILE = load(sprintf('seq_data/Feat-%s-Window%d-Stride%d.mat',opts.feats{i},opts.window,opts.stride));
        matObj{i} = (DATA_FILE);
        VID{i} = DATA_FILE.VID;%matObj{i}.VID;
    end
end

for seq_id1 = 1 : min(opts.videos,numel(dataset{1}.matnames)) 
    
    gt1 = getIkeaGTLablesForVIdeo(dataset{1},seq_id1);
    for i = 1 : numel(opts.feats)
        rankpool1{i} = matObj{i}.data(find(VID{i} == seq_id1),:);
        %rankpool1{i} = getNonLinearity(rankpool1{i},opts.postpoolnonlinear);
    end  
    
    nDimsX = getIkeaSequenceSize(opts.issareh,seq_id1);
    range1 = (opts.window-1)/2+1 : opts.stride : nDimsX -(opts.window-1)/2;
    
    for seq_id2 =  seq_id1+1: min(opts.videos,numel(dataset{1}.matnames)) 
        timest = tic();
        gt2 = getIkeaGTLablesForVIdeo(dataset{1},seq_id2);                  
        for i = 1 : numel(opts.feats)
            rankpool2{i} = matObj{i}.data(find(VID{i} == seq_id2),:);
            %rankpool2{i} = getNonLinearity(rankpool2{i},opts.postpoolnonlinear);
        end            
        
        nDimsX = getIkeaSequenceSize(opts.issareh,seq_id2);
        range2 = (opts.window-1)/2+1 : opts.stride : nDimsX-(opts.window-1)/2;
        
        clear nDimsX
        
        for i = 1 : numel(opts.feats)
            if i==1
                A1 = rankpool1{i};
                A2 = rankpool2{i};
            else
                A1 =  horzcat(A1,rankpool1{i});
                A2 =  horzcat(A2,rankpool2{i});
            end
        end
        
        [pr1,pr2] = matchTemporalSequences(A1,A2,opts.detections,opts.window, range1, range2 );        
        
        [detections2,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2);
        %detection2 = tempoalNonMaximaSuppression(detections2);
        precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
        recall(pair_id,1) = size(detections2,1)/size(pr1,1);        
        timest = toc(timest);
        detects{pair_id} = detections2;
        
        fprintf('[Baseline2 Eraly.Fusion][%s-W%d-S%d-D%d-%s-L%d-%s-%s][%d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
            opts.featstr,opts.window,opts.stride,opts.detections,opts.poolmethod,...
            seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
            mean(precision*100),mean(recall*100),timest); 
        
%         fprintf('[Matching][W%d-S%d-D%d][pair %d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
%             opts.window,opts.stride,opts.maxdetection,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
%             mean(precision*100),mean(recall*100),timest);                            
        pair_id = pair_id + 1;                          
        save(results_file,'precision','recall','pair_id','detects','seq_id1','seq_id2');  
    end    
end

