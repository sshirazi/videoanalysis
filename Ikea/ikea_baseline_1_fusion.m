%
%
% Clusterin based baseline.
%
%

function [expconfig] = ikea_baseline_1() 
    addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/utils';
%     addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/methods';
    addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/rankpooling';
    addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/code_public';
    addpath '/home/sareh/Codes/liblinear/matlab'
    %addpath '/home/sareh/Codes/libsvm/matlab'
    addpath '/home/sareh/Codes/Action_prediction_stuff/vlfeat/toolbox';
    vl_setup();


    opts = getOptimalConfiguration();
    [pr,rc,F] = main('clusternonlinearity',opts.clusternonlinearity,...
                                'clusterNorm',opts.clusterNorm,...
                                'poolnonlinear',opts.poolnonlinears,...
                                'poolnorm',opts.poolnorms,'poolmethod',opts.poolmethods,'islite',false);       

end

function opts = getOptimalConfiguration()
    clusternonlinearitys = {'none','ssr','chi1','chi2'};
    clusterNorms = {'none','L2'} ;
    poolnonlinears = {'none','ssr','chi1','chi2'};
    poolnorms = {'none','L2'} ;
    poolmethods = {'apr','rank'};
    id = 1;
    expconfig = [];
    if exist('expconfig_hog.mat','file'),load('expconfig_hog'); end
    for clusternonlinearity = clusternonlinearitys
        for clusterNorm = clusterNorms
            for poolnonlinear = poolnonlinears
                for poolnorm = poolnorms
                    for poolmethod = poolmethods
                        if (id-1)~= size(expconfig,2)
                            id = id + 1;
                        else
                            [pr,rc,F] = main('clusternonlinearity',clusternonlinearity{1},...
                                'clusterNorm',clusterNorm{1},...
                                'poolnonlinear',poolnonlinear{1},...
                                'poolnorm',poolnorm{1},'poolmethod',poolmethod{1});                    
                            expconfig(id).clusternonlinearity = clusternonlinearity{1};
                            expconfig(id).clusterNorm = clusterNorm{1};
                            expconfig(id).poolnonlinears = poolnonlinear{1};
                            expconfig(id).poolnorms = poolnorm{1};
                            expconfig(id).poolmethods = poolmethod{1};
                            expconfig(id).recall = rc;
                            expconfig(id).precision = pr;
                            expconfig(id).fscore = F;
                            save('expconfig_hog.mat','expconfig');
                            id = id + 1;
                        end
                    end
                end
            end
        end
    end
    for i = 1 :numel(expconfig) 
        f(i) = expconfig(i).fscore;
    end
    [val,indx] = max(f);
    
    opts.clusternonlinearity=  expconfig(indx).clusternonlinearity;
    opts.clusterNorm = expconfig(indx).clusterNorm;
    opts.poolmethods = expconfig(indx).poolmethods;
    opts.poolnonlinears = expconfig(indx).poolnonlinears;
    opts.poolnorms = expconfig(indx).poolnorms;
    
end

function [pr,rc,F] = main(varargin)

opts.feats           = {'hog','hof'};
opts.issareh        = true;

opts.numclusters    = 10;
opts.clusternonlinearity = 'none';
opts.clusterNorm         = 'none' ;
opts.window         = 21;
opts.coef           = 0.001;
opts.theshlen       = 20;

opts.poolmethod     = 'rank';
opts.poolnonlinear  = 'chi1';
opts.poolnorm       = 'none' ;

opts.maxdetection   = 200;
opts.islite         = false;

opts = bc_argparse(opts,varargin);

opts.pool.method    = opts.poolmethod;
opts.pool.nonlinear = opts.poolnonlinear;
opts.pool.norm      = opts.poolnorm    ;

opts.endvid = inf;
if opts.islite
    opts.endvid = 5;
end
opts
opts.pool
dataset = [];
opts.featstr = [];
for feat = opts.feats
    dataset{end+1} = getIkeaDataset(opts.issareh,feat{1});       
    opts.featstr = [opts.featstr feat{1} '-'];
end

allcandidates = generateCandidates(opts,dataset);
[pr,rc,F] = matchVideos(allcandidates,dataset,opts);

end

function [pr,rc,F] = matchVideos(allcandidates,dataset,opts)
    pair_id = 1;    
    for seq_id1 = 1 : min(numel(dataset{1}.matnames),opts.endvid) 
        for i = 1 : numel(opts.feats)
            if i == 1
                A1 = loadIkeaSequences(dataset{i},seq_id1);
            else
                A1 = horzcat(A1,loadIkeaSequences(dataset{i},seq_id1));
            end
        end
        gt1 = getIkeaGTLablesForVIdeo(dataset{1},seq_id1);                    
        out1 = getVideoEncodings(A1,allcandidates{seq_id1},opts);
        
        for seq_id2 =  seq_id1+1: min(opts.endvid,numel(dataset{1}.matnames)) 
            timest = tic();
            gt2 = getIkeaGTLablesForVIdeo(dataset{1},seq_id2);  
            for i = 1 : numel(opts.feats)
                if i == 1
                    A2 = loadIkeaSequences(dataset{i},seq_id2);
                else
                    A2 = horzcat(A2,loadIkeaSequences(dataset{i},seq_id2));
                end
            end
            out2 = getVideoEncodings(A2,allcandidates{seq_id2},opts);
            clear A1 A2
            M = out1 * out2';
            [pr1, pr2] = getDetections(M, allcandidates{seq_id1}, allcandidates{seq_id2}, opts.maxdetection) ;
            [detections2,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2);       
            
            precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
            recall(pair_id,1) = size(detections2,1)/size(pr1,1);                    
            detects{pair_id} = detections2;
            timest = toc(timest);
            fprintf('[BS1][W%d-D%d][pair %d vs %d][ Prec. %1.3f][ Rec. %1.3f][Mean Pr. %1.3f][Mean Rc. %1.3f][F = %1.3f][F mean = %1.3f][Time %f]\n',...
            opts.window,opts.maxdetection,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
            mean(precision*100),mean(recall*100), ...
            (200 * precision(pair_id,1) * recall(pair_id,1) / (recall(pair_id,1) + precision(pair_id,1)) ),...
            200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall))  ,timest);   
            if ~opts.islite 
                save(sprintf('results_%s__baseline_1.mat',opts.featstr),'precision','recall');
            end
            pair_id = pair_id + 1;
        end
    end    
    
    pr = mean(precision*100);
    rc = mean(recall*100);
    if mean(precision*100) == 0 && mean(recall*100) == 0
        F = 0;
    else
        F = 200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall));
    end
        fprintf('[Mean Pr. %1.3f][Mean Rc. %1.3f][F mean = %1.3f]\n',...            
            pr,rc,F );  
    
end
function [pr1, pr2] = getDetections(A,Candidate1,Candidate2,max_detections)   
    max_detections = min(max_detections,numel(find(A(:)>0.2)));
    pr1 = zeros(max_detections,2);
    pr2 = zeros(max_detections,2);
    for detection_id = 1 : max_detections
                [val,I] = max(A(:));                
                [I_row, I_col] = ind2sub(size(A),I);
                % get detection for video 1                
                pr1(detection_id,:) = Candidate1(I_row,:);                
                % get detection for video 2               
                pr2(detection_id,:) = Candidate2(I_col,:);                
                A(I_row,I_col) = -inf;
    end
end

function out = getVideoEncodings(data,candidates,opts)
    switch opts.pool.nonlinear
        case {'none','ssr'}
            dim = 2;
        case 'chi2exp'
            dim = 12;   
        case 'chi1'
            dim = 2;       
        case 'chi2'
            dim = 10;       
    
    end

    out = zeros(size(candidates,1),size(data,2)*dim,'single');
    for i = 1 : size(candidates,1)
        seq = data(candidates(i,1):candidates(i,2),:); 
        switch opts.pool.method
            case 'apr'
                out(i,:) = genAprRepresentation(seq,1,opts.pool.nonlinear)';
            case 'rank'
                out(i,:) = genRepresentation(seq,1,opts.pool.nonlinear)';
        end
%         if mod(i,80) == 0
%             fprintf('\n');
%         else
%             fprintf('.');
%         end
    end
%    fprintf('\n');    
    switch opts.pool.norm 
        case 'L2'
            out = normalizeL2(out);
        case 'none'
            out = out;
        case 'default'
            out = out;
    end
            
end

function allcandidates = generateCandidates(opts,dataset)
at = harmonic(1:opts.window);
if ~strcmp(opts.feats,'deep')
    M = repmat(at',1,4000);
else 
    M = repmat(at',1,2048);
end
if opts.islite
    sfx = 'lite';
else
    sfx = '';
end
FILE = sprintf('candidates/%s%s-%s-Norm-feat%s-C%d-W%d-L%d.mat',...
    sfx,opts.clusternonlinearity,opts.clusterNorm,opts.featstr,opts.numclusters,...
    opts.window,opts.theshlen );
st = 1;
if exist(FILE,'file')==2
    load(FILE);    
    st = numel(allcandidates)+ 1;
end
for seq_id1 = st : min(opts.endvid,numel(dataset{1}.matnames)) 
    for i = 1 : numel(opts.feats)
        tdata = loadIkeaSequences(dataset{i},seq_id1) ;          
        tdata = tempPool(tdata,M);    
        tdata = getNonLinearity(tdata,opts.clusternonlinearity);
        optsval = 0;
        switch opts.clusterNorm
            case 'L2'
                tdata = normalizeL2(tdata);
                optsval = 1;
            case 'none'
                temp = tdata * tdata';
                optsval = mean(temp(:)); clear temp;
        end
        data{i} = [tdata opts.coef*optsval/size(tdata,1)*(1:size(tdata,1))']; 
    end
    
    for i = 1 : numel(opts.feats)
            if i==1
                A = data{i};
            else
                A =  horzcat(A,data{i});
            end
    end
    
    
    [~, idxkmean] = vl_kmeans(A' , opts.numclusters, 'algorithm', 'elkan', 'MaxNumIterations', 100) ;     
    changes = getChange(idxkmean);
    ends = find(changes~=0);
    detections = zeros(numel(ends)+1,2);
    for i = 1 : numel(ends)
        if i == 1
            detections(i,1) = 1;
            detections(i,2) = ends(i);
        else
            detections(i,1) = ends(i-1)+1;
            detections(i,2) = ends(i);
        end
    end    
    detections(end+1,1:2) = [ends(i) numel(idxkmean)];    
    candidates = [];
    for i = 1 : size(detections,1)
        if detections(i,2) - detections(i,1) > opts.theshlen
            candidates = [ candidates ; detections(i,:) ];
        end
    end
    fprintf('[%d] [candidates %d]\n',seq_id1,size(candidates,1));
    allcandidates{seq_id1} = candidates;
    save(FILE,'allcandidates');    
end


end

function out = tempPool(data,M)    
    N = size(data,1);
    W = size(M,1);
    range = (W-1)/2:N-(W-1)/2-1;
    out = zeros(numel(range),size(data,2),'single');
    for i = 1 : numel(range)
        r = range(i)-((W-1)/2)+1:range(i)+((W-1)/2)+1;
        out(i,:) = sum(data(r,:).*M);
        %out(i,:) = genRepresentation(data(r,:),1,'ssr');
        if mod(i,80) == 0, fprintf('.'); end
        if mod(i,6400) == 0, fprintf('\n'); end
    end
    fprintf('\n');
end

function a = getChange(in)
    a = double(in(2:end)) - double(in(1:end-1));
end