seq1 = 1;
seq2 = 2;

load(fullfile('/home/sareh/Datasets/IKEA/SalinteFeats',sprintf('%s.mat',seq1)));
feats1 = feats;
load(fullfile('/home/sareh/Datasets/IKEA/SalinteFeats',sprintf('%s.mat',seq2)));
feats2 = feats;
clear feats;

compareSequences(feats1,feats2);