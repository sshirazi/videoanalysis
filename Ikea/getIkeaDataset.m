function dataset = getIkeaDataset(issareh,bowfeat)

if issareh 
    dataset.videodir = '';
    dataset.framedir = '/home/sareh/Datasets/IKEA/tmp/data';
    dataset.bowfeatdir  = sprintf('/home/sareh/Datasets/IKEA/tmp/feats/%s',bowfeat);
    IKEA_DATASET_PATH = '/home/sareh/Datasets/IKEA/';
else
    dataset.videodir = '';
    dataset.framedir = '';
    dataset.bowfeatdir  = '';
    IKEA_DATASET_PATH = '';
end

dataset.clipnames       = textread(fullfile(IKEA_DATASET_PATH,'videoNumbers(copy).csv'),'%s');
dataset.clipannotations = csvread(fullfile(IKEA_DATASET_PATH,'temporal.csv'));
dataset.videonames      = unique(dataset.clipnames);
dataset.dbpath          = IKEA_DATASET_PATH;

for i = 1 : numel(dataset.videonames)
    a = dataset.videonames{i};
    if ~strcmp(bowfeat,'deep')
            dataset.matnames{i} = sprintf('%s-bow',a);
            dataset.runs{i} = sprintf('%s',a);
    else
            dataset.matnames{i} = sprintf('%s',a);
            dataset.runs{i} = sprintf('%s',a);
    end
end
end
