% Rank pooling main function
%
% Author: Basura Fernando
function W = genRepresentation_alpha(data,CVAL,nonLin,alpha)
    %%% Apply ARMA
    Data(1,:) = data(1,:);
    for i = 2:size(data,1)
        Data(i,:) = alpha * Data(i-1,:) + (1 - alpha) * data(i,:);
    end
    W_fow = liblinearsvr(getNonLinearity(Data,nonLin),CVAL,2); clear Data; 			
    order = 1:size(data,1);
    [~,order] = sort(order,'descend');
    data = data(order,:);
    %%% Apply ARMA
    Data(1,:) = data(1,:);
    for i = 2:size(data,1)
        Data(i,:) = alpha * Data(i-1,:) + (1 - alpha) * data(i,:);
    end
    W_rev = liblinearsvr(getNonLinearity(Data,nonLin),CVAL,2); 			              
    W = [W_fow ; W_rev]; 
end