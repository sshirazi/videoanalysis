function data = loadIkeaSequences(dataset,id)    
    load(fullfile(dataset.bowfeatdir,sprintf('%s.mat',dataset.matnames{id})));
    %obj = VideoReader(fullfile(dataset.dbpath,'videos',sprintf('%s.avi',dataset.videonames{id})));
    if ~exist('histv')
        histv = feats;
    end
    nFrames  = size(histv,1);
    data = zeros(size(histv),'single');
    for i = 2 : nFrames
        data(i,:) = histv(i,:) - histv(i-1,:);
    end
    data(1,:) = histv(1,:);
    %data  = data';
end

