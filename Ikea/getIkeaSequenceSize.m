function nDimsX = getIkeaSequenceSize(issareh,id)
    if nargin < 1, issareh = false; end
    dataset = getIkeaDataset(issareh,'hof');        
    matObj = matfile(fullfile(dataset.bowfeatdir,sprintf('%s.mat',dataset.matnames{id})));
    info = whos(matObj,'histv');
    nDimsX = info.size(2);    
end