
%
% This baseline just apply rank pooling and do the matching without any
% temporal consistency search
%
%
function ikea_baseline_2(varargin)
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/code_public';
addpath '/home/sareh/Codes/liblinear/matlab'
%addpath '/home/sareh/Codes/libsvm/matlab'
addpath '/home/sareh/Codes/Action_prediction_stuff/vlfeat/toolbox';
vl_setup();
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/utils';
addpath '/home/sareh/Codes/Action_prediction_stuff/VideoAnalysis/shared_code/videoanalysis/methods';

opts.feat = 'hog';
opts.issareh = true;
opts.window  = 41;
opts.stride  = 20;
opts.poolmethod = 'rank';
opts.maxdetection = 200;
opts.threshold = 0.2;
opts = bc_argparse(opts,varargin);

dataset = getIkeaDataset(opts.issareh,'hog');       
pair_id = 1;
results_file = sprintf('results-%s-W%d-S%d-ND%d-chi2exp-%s-chi2exp-matching-THRESH%1.2f.mat',opts.feat,...
    opts.window,opts.stride,opts.maxdetection,opts.poolmethod,opts.threshold);
if exist(results_file,'file')
    sprintf('\n\n\n****Result file already exist! Please check before running!****\n\n\n')
    error('******Result file already exist! Please check before running*****');
end
DATA_FILE = sprintf('seq_data/Feat-%s-Window%d-Stride%d.mat',opts.feat,opts.window,opts.stride);
matObj = matfile(DATA_FILE);
VID = matObj.VID;

for seq_id1 = 1 : numel(dataset.matnames) 
    
    rankpool1 = matObj.data(find(VID ==  seq_id1),:);    
    gt1 = getIkeaGTLablesForVIdeo(dataset,seq_id1);    
    nDimsX = getIkeaSequenceSize(opts.issareh,seq_id1);
    range1 = (opts.window-1)/2+1 : opts.stride : nDimsX -(opts.window-1)/2;
    
    for seq_id2 =  seq_id1+1: numel(dataset.matnames) 
        timest = tic();        
        
        nDimsX = getIkeaSequenceSize(opts.issareh,seq_id2);
        range2 = (opts.window-1)/2+1 : opts.stride : nDimsX-(opts.window-1)/2;
        rankpool2 = matObj.data(find(VID == seq_id2),:);
        gt2 = getIkeaGTLablesForVIdeo(dataset,seq_id2);
        
        [pr1,pr2] = matchTemporalSequences(rankpool1,rankpool2,opts.maxdetection,opts.window, range1, range2 );        
        
        [detections2,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2);
        %detection2 = tempoalNonMaximaSuppression(detections2);
        precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
        recall(pair_id,1) = size(detections2,1)/size(pr1,1);        
        timest = toc(timest);
        detects{pair_id} = detections2;
                
        fprintf('[Matching][W%d-S%d-D%d][pair %d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
            opts.window,opts.stride,opts.maxdetection,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
            mean(recall*100),mean(precision*100),timest);                            
        pair_id = pair_id + 1;                          
        save(results_file,'precision','recall','pair_id','detects','seq_id1','seq_id2');  
    end    
end

