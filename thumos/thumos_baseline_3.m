function thumos_baseline_3(varargin)

opts.window     = 61;
opts.len        = 12;
opts.dec        = 50;    
opts.feat       = 'resnet-152';
opts.matching   = '';
opts.isnci      = false;
opts = bc_argparse(opts,varargin);
pid = feature('getpid');
opts.logFileName = sprintf('../logs/thumos_baseline_3/det%d-len%d-win%d-pid%d.log',opts.dec,opts.len,opts.window,pid);
bmkdir(opts.logFileName);

%try
[pr,rc,F] = main('window',opts.window ,'stride',10,'detections',opts.dec,'len',opts.len,...
                     'prepoolnonlinear','none','postpoolnonlinear','none'...
                     ,'postpoolnorm','L2','poolmethod','ARMARank',...
                     'islite',false,'feat',opts.feat,'logFileName',...
                     opts.logFileName,'matching',opts.matching,'isnci',opts.isnci ); 
%   catch err
%    fid = fopen(opts.logFileName,'a');
%    fprintf(fid,'ERROR\n');
%    fprintf(fid,'%s\n',err.identifier);
%    fprintf(fid,'%s\n',err.message);
%    fprintf(fid,'file = %s \n function  = %s\n Line =  %d\n',char(err.stack(end).file),err.stack(end).name,err.stack(end).line);
%    fclose(fid);
%    errFile = sprintf('../err/thumos_baseline_3/det%d-len%d-win%d-pid%d',opts.dec,opts.len,opts.window,pid);
%    bmkdir(errFile);
%    copyfile(opts.logFileName,errFile);
%    delete(opts.logFileName);   
%    exit();   
% end    
exit();
end


function [pr,rc,F] = main(varargin)
opts.feat       = 'resnet-152'; % feature name
opts.issareh    = false;        % use for sareh
opts.isnci      = false;        % use for nci processing
opts.islite     = true;         % lite version is for faster experiments
opts.id         = 0;            % experiment id

opts.window     = 61;           
opts.stride     = 10;
opts.detections = 50;
opts.poolmethod = 'ARMARank';  % ARMARank   rank
opts.len        = 10;
opts.nms        = true;      % non maxima supression
opts.alpha      = 0.6;
opts.matching   = '';

opts.prepoolnonlinear = 'none';
opts.postpoolnonlinear = 'none';
opts.postpoolnorm = 'L2';
opts.logFileName   = '';
opts = bc_argparse(opts,varargin);
[~,hostname] = system('hostname');
hostname = strtrim(hostname);
opts.hostname = hostname;
opts.uiothreshold = 0.5;

issareh = opts.issareh;
WindowSize = opts.window;
Stride     = opts.stride;
NUM_DETECTIONS  = opts.detections; 
POOL_METHOD     = opts.poolmethod;
dataset =  getThumosDataset(issareh,opts.feat);  
if opts.islite
    dataset.pairs = dataset.pairs(1:5,:);
end


if opts.nms == false
    if isempty(opts.matching)
        results_file = sprintf('../detections-thumos/results/results-%s-W%d-S%d-ND%d-%s-%s-%s-greedy-LEN%d-Norm%s.mat',opts.feat,...
            WindowSize,Stride,NUM_DETECTIONS,opts.prepoolnonlinear,...
            POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm);
    else
        results_file = sprintf('../detections-thumos/results/results-%s-W%d-S%d-ND%d-%s-%s-%s-greedy-LEN%d-Norm%s-matching%s.mat',opts.feat,...
            WindowSize,Stride,NUM_DETECTIONS,opts.prepoolnonlinear,...
            POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm,opts.matching);
    end
else
     if isempty(opts.matching)
        results_file = sprintf('../detections-thumos/results/results-%s-W%d-S%d-ND%d-%s-%s-%s-greedy-LEN%d-Norm%s-nms.mat',opts.feat,...
        WindowSize,Stride,NUM_DETECTIONS,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm);
     else
         results_file = sprintf('../detections-thumos/results/results-%s-W%d-S%d-ND%d-%s-%s-%s-greedy-LEN%d-Norm%s-nms-matching%s.mat',opts.feat,...
        WindowSize,Stride,NUM_DETECTIONS,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm,opts.matching);
     end
end
bmkdir(results_file);
if exist(results_file,'file') == 2
    load(results_file);
    pr = mean(precision*100);
    rc = mean(recall*100);
    F = 200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall));
    fprintf('precision  = %1.4f\n',pr);
    fprintf('recall     = %1.4f\n',rc);
    fprintf('fscore     = %1.4f\n',F);    
    return;
else
   %pr = -1; rc = -1; F = -1;
   %return;
end

if opts.isnci == true
    detectMain = '/short/ra4/bxf660';
else
    detectMain = '..';
end

if isempty(opts.matching)
    detection_file = sprintf('%s/detections-thumos/alldetections/baseline3-%s-W%d-S%d-Pre%s-Pool%s-Post%s-LEN%d-Norm%s.mat',detectMain,opts.feat,...
        WindowSize,Stride,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm);
else
    detection_file = sprintf('%s/detections-thumos/alldetections/baseline3-%s-W%d-S%d-Pre%s-Pool%s-Post%s-LEN%d-Norm%s-matching%s.mat',detectMain,opts.feat,...
        WindowSize,Stride,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm,opts.matching);
end
bmkdir(detection_file);

if exist(detection_file,'file') == 2 && ~opts.islite
    [pr,rc, F] = callGetResults(detection_file,opts,results_file,dataset);
    fprintf('precision  = %1.4f\n',pr);
    fprintf('recall     = %1.4f\n',rc);
    fprintf('fscore     = %1.4f\n',F);
    pid = feature('getpid');
    outFile = sprintf('../out/thumos_baseline_3/det%d-len%d-win%d-pid%d',NUM_DETECTIONS,opts.len,opts.window,pid);
    bmkdir(outFile);
    copyfile(opts.logFileName,outFile);
    delete(opts.logFileName);
    return;
end

if isempty(opts.matching)
    detection_temp_file = sprintf('%s/detections-thumos/alldetections/temp-baseline3-%s-W%d-S%d-Pre%s-Pool%s-Post%s-LEN%d-Norm%s.mat',detectMain,opts.feat,...
        WindowSize,Stride,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm);
else
    detection_temp_file = sprintf('%s/detections-thumos/alldetections/temp-baseline3-%s-W%d-S%d-Pre%s-Pool%s-Post%s-LEN%d-Norm%s-matching%s.mat',detectMain,opts.feat,...
        WindowSize,Stride,opts.prepoolnonlinear,POOL_METHOD,opts.postpoolnonlinear,opts.len,opts.postpoolnorm,opts.matching);
end


DATA_FILE = generateTemporalEncodesAndSave('feat',opts.feat,'issareh',...
    opts.issareh,'window',opts.window,'stride',opts.stride,'prepoolnonlinear',...
    opts.prepoolnonlinear,'poolmethod',opts.poolmethod,'isnci',opts.isnci,'hostname',opts.hostname,'alpha',opts.alpha,'logFileName',opts.logFileName);
matObj = matfile(DATA_FILE);
VID = matObj.VID;

if exist(detection_temp_file,'file') == 2
    load(detection_temp_file);
else
    pair_id = 1;    
end
for pair_st = pair_id : size(dataset.pairs,1)
    pair = dataset.pairs(pair_st,:);
    timest = tic();
    seq_id1 = pair(1);
    seq_id2 = pair(2);
    gt1 = getTHUMOSGTLablesForVIdeo(dataset,seq_id1);    gt1 = gt1 .* 30;    
    rankpool1 = matObj.data(find(VID == seq_id1),:);    
    nDimsX = getThumosSequenceSize(seq_id1,dataset);
    range1 = (WindowSize-1)/2+1 : Stride : nDimsX -(WindowSize-1)/2;
    rankpool1 = getNonLinearity(rankpool1,opts.postpoolnonlinear);
    if strcmp(opts.postpoolnorm,'L2')           
        rankpool1 = normalizeL2(rankpool1);
    end
    
    gt2 = getTHUMOSGTLablesForVIdeo(dataset,seq_id2); gt2 = gt2 .* 30;    
    rankpool2 = matObj.data(find(VID == seq_id2),:);      
    nDimsX = getThumosSequenceSize(seq_id2,dataset); 
    range2 = (WindowSize-1)/2+1 : Stride : nDimsX-(WindowSize-1)/2;       
    rankpool2 = getNonLinearity(rankpool2,opts.postpoolnonlinear);        
    if strcmp(opts.postpoolnorm,'L2')           
        rankpool2 = normalizeL2(rankpool2);
    end               
    switch opts.matching
        case ''
            A = rankpool1 *rankpool2';
         case 'sa'
            sdim = 50; 
            Xs = pca(rankpool1);
            Xt = pca(rankpool2);
            Xs = Xs(:,1:sdim);
            Xt = Xt(:,1:sdim);
            A = rankpool1 * ((Xs * Xs') * (Xt * Xt')) * rankpool2';    
    end
    
    [M,J] = getCandiadateMatrix(A);
    [detection] = getAllDetections(opts,J);       
    pr1 = zeros(size(detection,1),2); pr2 = pr1;
    for di = 1 : size(detection,1)            
            ri = detection(di,1);
            k  = detection(di,3);
            ci = detection(di,2);            
            pr1(di,1) = range1(ri) - (WindowSize-1)/2;
            pr1(di,2) = range1(ri+k) + (WindowSize-1)/2;
            pr2(di,1) = range2(ci) - (WindowSize-1)/2;
            pr2(di,2) = range2(ci+k) + (WindowSize-1)/2;                
    end  
    if size(detection,1) > 0
        alldetections{pair_id}.detections = [pr1 pr2 detection(:,4)];
        alldetections{pair_id}.files{1} = dataset.matnames{seq_id1};
        alldetections{pair_id}.files{2} = dataset.matnames{seq_id2};
    end
    if ~isempty(detection)
        detection = tempoalNonMaximaSuppression(alldetections{pair_id}.detections);
        [scorevals,scoreindx] = sort(detection(:,5),'descend');
        pr1 = []; pr2 = [];
        pr1 = []; pr2 = [];
        for di = 1 : min(opts.detections,size(detection,1))                        
            pr1(di,:) = detection(scoreindx(di),1:2);                
            pr2(di,:) = detection(scoreindx(di),3:4);
        end      
        [detections2,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,[],opts.uiothreshold);
        precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
        recall(pair_id,1) = size(detections2,1)/size(pr1,1);        
        timest = toc(timest);
        detects{pair_id} = detections2;
    else
        precision(pair_id,1) = 0;
        recall(pair_id,1) = 0;
        detects{pair_id} =[];
    end       
        
    fprintf('[%s %d][%s-W%d-S%d-D%d-%s-L%d-%s-%s-%s][%d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
        opts.hostname, opts.id,opts.feat,opts.window,opts.stride,opts.detections,opts.poolmethod,opts.len,...
        opts.prepoolnonlinear,opts.postpoolnonlinear,opts.postpoolnorm,...
        seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
        mean(precision*100),mean(recall*100),timest); 
    
    fid = fopen(opts.logFileName,'a');
    fprintf(fid,'[%s][%s %d][%s-W%d-S%d-D%d-%s-L%d-%s-%s-%s][%d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
        datestr(now),opts.hostname, opts.id,opts.feat,opts.window,opts.stride,opts.detections,opts.poolmethod,opts.len,...
        opts.prepoolnonlinear,opts.postpoolnonlinear,opts.postpoolnorm,...
        seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
        mean(precision*100),mean(recall*100),timest); 
    fclose(fid);
    pair_id = pair_id + 1;    
    save(detection_temp_file,'pair_id','alldetections','precision','recall','detects','-v7.3');
end 

if ~opts.islite
    save(detection_file,'alldetections','-v7.3');  
    save(results_file,'precision','recall','pair_id','detects','seq_id1','seq_id2');              
end
pr = mean(precision*100);
rc = mean(recall*100);
F = 200* mean(precision) * mean(recall)/ (mean(precision) + mean(recall));
fprintf('precision  = %1.4f\n',pr);
fprintf('recall     = %1.4f\n',rc);
fprintf('fscore     = %1.4f\n',F);
pid = feature('getpid');
outFile = sprintf('../out/thumos_baseline_3/det%d-len%d-win%d-pid%d',opts.detections,opts.len,opts.window,pid);
bmkdir(outFile);
copyfile(opts.logFileName,outFile);
delete(opts.logFileName);
end

function DATA_FILE = generateTemporalEncodesAndSave(varargin)
    
    opts.feat               = 'hof';
    opts.issareh            = false;
    opts.window             = 61;
    opts.stride             = 50;
    opts.prepoolnonlinear   = 'ssr';
    opts.poolmethod         = 'rank';
    opts.isnci              = false;
    opts.hostname           = '';
    opts.alpha              = 0.1;
    opts.logFileName        = 'a.log' ;
    opts = bc_argparse(opts,varargin);
    
    issareh = opts.issareh;
    WindowSize = opts.window;
    Stride     = opts.stride;

    dataset = getThumosDataset(issareh,opts.feat);
    if strcmp(opts.hostname,'basura-pc')
        MAIN_DIR = '/home/basura/server/home/basura';        
    else
        MAIN_DIR = '/short/ra4/bxf660';        
    end  
    
    DATA_FILE = sprintf('%s/THUMOS_CVPR17_Seq_Data/Feat-%s-Window%d-Stride%d-Pool%s-Rank%s.mat',...
            MAIN_DIR,opts.feat,WindowSize,Stride,opts.prepoolnonlinear,opts.poolmethod);
    
        

    switch opts.prepoolnonlinear
        case {'none','ssr'}
            dim = 2;
        case 'chi2exp'
            dim = 12;   
        case 'chi1'
            dim = 6;       
        case 'chi2'
            dim = 10;           
    end
    if exist(DATA_FILE,'file') ~= 2
        count = 0;
        for seq_id1 = 1 : numel(dataset.matnames)
            nDimsX = getThumosSequenceSize(seq_id1,dataset);
            count = count + numel((WindowSize-1)/2+1 : Stride : nDimsX-(WindowSize-1)/2);   
            fprintf('[%d ] [counting elements to cluster %d ]\n',seq_id1,count);
        end
        data = zeros(count,2048*dim,'single');
        VID = zeros(count,1,'single');
        st = 1;
        for seq_id1 = 1 : numel(dataset.matnames)
            timest = tic();
            feats = loadThumosSequences(dataset,seq_id1);   
            range = (WindowSize-1)/2+1 : Stride : size(feats,1)-(WindowSize-1)/2;
            rankpool = getEncode(feats,'window',opts.window,'stride',opts.stride,...
                'prepoolnonlinear',opts.prepoolnonlinear,'poolmethod',opts.poolmethod,'alpha',opts.alpha);
            data(st:st+numel(range)-1,:) = rankpool;
            VID(st:st+numel(range)-1,:) = seq_id1;
            st = st + numel(range);
            timest = toc(timest);
            fprintf('[%d ] [Frames %d ] [ Fps %1.1f] [Time %1.1f]\n',seq_id1,size(feats,1),size(feats,1)/timest,timest);    
            fid = fopen(opts.logFileName,'a');
            fprintf(fid,'[%d ] [Frames %d ] [ Fps %1.1f] [Time %1.1f]\n',seq_id1,size(feats,1),size(feats,1)/timest,timest);    
            fclose(fid);
            
        end
        save(DATA_FILE,'data','VID','-v7.3');   
    end
end

function [rankpool] = getEncode(feats,varargin)

opts.window  = 61;
opts.stride  = 50;
opts.prepoolnonlinear = 'ssr';
opts.poolmethod = 'rank';
opts.alpha = 0.6;
opts = bc_argparse(opts,varargin);

W = opts.window;
S = opts.stride;

range = (W-1)/2+1 : S : size(feats,1)-(W-1)/2;
nFrm = numel(range);
switch opts.prepoolnonlinear
    case {'none','ssr'}
        dim = 2;
    case 'chi2exp'
        dim = 12;   
    case 'chi1'
        dim = 6;       
    case 'chi2'
        dim = 10;      
     case 'ser'
        dim = 4;       
end
rankpool = zeros(nFrm,size(feats,2)*dim,'single');
for p =1 : numel(range)
   i = range(p); 
   if i == range(end) 
       data = feats(i - (W-1)/2 : max(i+(W-1)/2,size(feats,1)),:);   
   else
       data = feats(i - (W-1)/2 : i+(W-1)/2,:);   
   end
   switch opts.poolmethod
       case 'rank'
           rankpool(p,:) = genRepresentation(data,1,opts.prepoolnonlinear);
       case 'apr'
           rankpool(p,:) = genAprRepresentation(data,1,opts.prepoolnonlinear);
       case 'ARMARank'    
           rankpool(p,:) = genRepresentation_alpha(data,1,opts.prepoolnonlinear,opts.alpha);
   end
end

end

function  [pr,rc,F] = callGetResults(detection_file,opts,results_file,dataset)
load(detection_file);
pair_id = 1;
for pair = dataset.pairs'
    timest = tic();
    seq_id1 = pair(1);
    seq_id2 = pair(2);
    gt1 = getTHUMOSGTLablesForVIdeo(dataset,seq_id1);    gt1 = gt1 .* 30;          
    gt2 = getTHUMOSGTLablesForVIdeo(dataset,seq_id2);    gt2 = gt2 .* 30;          
    if  exist('alldetections','var') == 1 && ~isempty(alldetections) && ~isempty(alldetections{pair_id})
        detection = alldetections{pair_id}.detections;
        detection = tempoalNonMaximaSuppression(detection);
    else
            detection = [];
    end
    if ~isempty(detection)
        [scorevals,scoreindx] = sort(detection(:,5),'descend');
        pr1 = []; pr2 = [];
        for di = 1 : min(opts.detections,size(detection,1))                        
            pr1(di,:) = detection(scoreindx(di),1:2);                
            pr2(di,:) = detection(scoreindx(di),3:4);
        end      
        [detections2,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,[],opts.uiothreshold);
        precision(pair_id,1) = size(detections2,1)/size(gtdetection,1);
        recall(pair_id,1) = size(detections2,1)/size(pr1,1);        
        timest = toc(timest);
        detects{pair_id} = detections2;
    else
        precision(pair_id,1) = 0;
        recall(pair_id,1) = 0;
        detects{pair_id} =[];
    end
    fprintf(['[Greedy %d][%s-W%d-S%d-D%d-%s-L%d-%s-%s-%s][%d vs %d]'...
    '[ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n'],...
    opts.id,opts.feat,opts.window,opts.stride,opts.detections,opts.poolmethod,opts.len,...
    opts.prepoolnonlinear,opts.postpoolnonlinear,opts.postpoolnorm,...
    seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
    mean(precision*100),mean(recall*100),timest);   

     fid = fopen(opts.logFileName,'a');
     fprintf(fid,['[%s][%s][BS3 %d][%s-W%d-S%d-D%d-%s-L%d-%s-%s-%s][%d vs %d]'...
    '[ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n'],...
    datestr(now),opts.hostname,opts.id,opts.feat,opts.window,opts.stride,opts.detections,opts.poolmethod,opts.len,...
    opts.prepoolnonlinear,opts.postpoolnonlinear,opts.postpoolnorm,...
    seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
    mean(precision*100),mean(recall*100),timest);                       
    fclose(fid);
    
    pair_id = pair_id + 1;         
end
pr = mean(precision) * 100;
rc = mean(recall) * 100;
F = (pr*rc*2)/(pr + rc);
if ~opts.islite
            save(results_file,'precision','recall','pair_id','detects','seq_id1','seq_id2');              
end
end

function [M,J] = getCandiadateMatrix(A)
        % hueristic greedy method
        M = A;
        meanA = mean(A(:));
        stdA = std(A(:));
        THRESH = 0;
        % Find high confident pairwise similarity
        M(find(M < (THRESH))) = 0;     
        % Initialize the final matching matrix
        J = zeros(size(M));        
        % enumulate each element in the left bi-partite graph
        LEN = 2;
        for ri = 1 : size(M,1)-LEN       
            % find the connected components for the node ri 
            ri_col_indx = find(M(ri,:)>0);
            % initialize candidates
            possible = ri_col_indx;
            % Now iterate for minimum depth of LEN
            for k = 1 : LEN
                ri_next_col_indx = find(M(ri+k,:)>0);
                possible = intersect(possible,ri_next_col_indx-k);
            end
            for k = 1 : LEN
                J(ri+k-1,possible+k-1) = A(ri+k-1,possible+k-1);                
            end
            eliminate = ones(1,size(M,2));
            eliminate(possible) = 0;
            M(ri,find(eliminate==1)) = 0;           
        end               
end

function [detection] = getAllDetections(opts,J)
    detection = [];        
    det_id = 1;
    for ri = 1 : size(J,1)-1            
        ri_col_indx = find(J(ri,:)>0);
        for col_indx = 1 : numel(ri_col_indx)
            found_it = false;
            k = 1;
            score = J(ri,ri_col_indx(col_indx));
            while ~found_it
                if(J(ri+k,ri_col_indx(col_indx)+k)) == 0
                    found_it = true;
                    J(ri,ri_col_indx(col_indx)) = 0;
                else
                    score = score + J(ri+k,ri_col_indx(col_indx)+k);
                    J(ri+k,ri_col_indx(col_indx)+k) = 0;
                end
                k = k + 1;
            end
            k = k -1;
            if k >= opts.len
                detection(det_id,1) = ri;
                detection(det_id,2) = ri_col_indx(col_indx);
                detection(det_id,3) = k;
                detection(det_id,4) = score/k;                             
                det_id = det_id + 1;
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%% Experiments %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function greedyexpconfig = fullSearch()
windows = [ 31 61 91];
strides = [ 10 30 60 ];
detectionss = [50 100 200 500 1000];
poolmethods = {'apr','rank'};
lens = [2 5 10 ];
prepoolnonlinears = {'none','ssr','chi2exp'};
postpoolnonlinears = {'none','ssr','chi2exp'};
postpoolnorms = {'none','L2'};

greedyexpconfig = [];
if exist('greedyexpconfig.mat','file'),load('greedyexpconfig'); end
id = 1;
for window = windows
    for stride = strides
        for detections = detectionss
            for poolmethod = poolmethods
                for len = lens
                    for prepoolnonlinear = prepoolnonlinears
                        for postpoolnonlinear  =  postpoolnonlinears
                            for postpoolnorm = postpoolnorms
                                 if (id-1)~= size(greedyexpconfig,2)
                                    id = id + 1;
                                 else
                                      if strcmp(prepoolnonlinear{1},'chi2exp')...
                                          || strcmp(postpoolnonlinear{1},'chi2exp')
                                            pr=-1;rc=-1;F=-1;
                                      else
    [pr,rc,F] = main('window',window,'stride',stride,'detections',detections,...
        'poolmethod',poolmethod{1},'len',len,'prepoolnonlinear',prepoolnonlinear{1},...
        'postpoolnonlinear',postpoolnonlinear{1},'postpoolnorm',postpoolnorm{1},'id',id);
                                      end
    
                        greedyexpconfig(id).window = window;
                        greedyexpconfig(id).stride = stride;
                        greedyexpconfig(id).detections = detections;
                        greedyexpconfig(id).poolmethod = poolmethod{1};
                        greedyexpconfig(id).len = len;
                        greedyexpconfig(id).prepoolnonlinear = prepoolnonlinear{1};
                        greedyexpconfig(id).postpoolnonlinear = postpoolnonlinear{1};
                        greedyexpconfig(id).postpoolnorm = postpoolnorm{1};
                        
                        greedyexpconfig(id).recall = rc;
                        greedyexpconfig(id).precision = pr;
                        greedyexpconfig(id).fscore = F;
                        save('greedyexpconfig.mat','greedyexpconfig');
                        id = id + 1;
                                 end
    
                            end
                        end
                    end
                end
            end
        end
    end
end



end

function interestingExperiment(feat)
    window = [31 61 91];
    len    = [2 5 10];
    for w = 1 : numel(window)
        for  l = 1 : numel(len)
        [pr,rc,F] = main('window',window(w),'stride',10,'detections',100,'len',len(l),...
                     'prepoolnonlinear','none','postpoolnonlinear','none'...
                     ,'postpoolnorm','L2','poolmethod','rank',...
                     'islite',false,'feat',feat); 
        end
    end
end

function only(feat)   
    
        [pr,rc,F] = main('window',61,'stride',10,'detections',100,'len',10,...
                     'prepoolnonlinear','none','postpoolnonlinear','none'...
                     ,'postpoolnorm','L2','poolmethod','rank',...
                     'islite',false,'feat',feat); 
    
end