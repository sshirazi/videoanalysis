function [uvidds,annotations,pairs] = getThumosAnnotations(issareh)
    if nargin < 1
        issareh = 0;
    end
    if issareh == 0
        gtpath = '../TH15_Temporal_annotations_validation/annotations'; 
        classIndexFile = '../TH15_Temporal_annotations_validation/Class Index_Detection.txt';
    else
        gtpath = '../TH15_Temporal_annotations_validation/annotations'; 
        classIndexFile = '../TH15_Temporal_annotations_validation/Class Index_Detection.txt';
    end
    
    subset = 'validation';
    [~,th14classnames]=textread(classIndexFile,'%d%s');    
    all = [];
    for i=1:length(th14classnames)
        class=th14classnames{i};
        gtfilename=[gtpath '/thumos15_temporal' subset '_' class   '.txt'];
        if exist(gtfilename,'file')~=2
            error(['TH14evaldet: Could not find GT file ' gtfilename])
        end
        [videonames,t1,t2]=textread(gtfilename,'%s%f%f');
        uvids = unique(videonames);        
        all = [all ; uvids];        
    end
    uvidds = unique(all);
    annotations = [];
    for i=1:length(th14classnames)
        class=th14classnames{i};
        gtfilename=[gtpath '/thumos15_temporal' subset '_' class   '.txt'];
        if exist(gtfilename,'file')~=2
            error(['TH14evaldet: Could not find GT file ' gtfilename])
        end
        [videonames,t1,t2]=textread(gtfilename,'%s%f%f'); 
        for j = 1 : numel(videonames)
            IndexC = strfind(uvidds, videonames{j});
            Index = find(not(cellfun('isempty', IndexC)));
            annotations = [annotations ; [Index t1(j) t2(j) i]];
        end
    end
    
    [pairs,counts] = findValid(uvidds,annotations);
     
   
    
end

function [pairs,counts] = findValid(uvidds,annotations)
    pair_id = 1;
    N = numel(uvidds);
    counts = zeros(1,(N*(N-1))/2);
    pairs = [];    
    for i = 1 : N
        indx_i = find(annotations(:,1) == i);
        class_i = annotations(indx_i,4);
        for j = i + 1 : N
            indx_j = find(annotations(:,1) == j);
            class_j = annotations(indx_j,4);
            k = 0;
            for ci = class_i'
                for cj = class_j'
                  k = k + (ci == cj);  
                end
            end
            counts(pair_id) = k;
            pair_id = pair_id + 1;
            if k >= 2
                pairs = [pairs ; [i j]];
            end
        end
    end
end