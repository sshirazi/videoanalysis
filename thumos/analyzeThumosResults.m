function analyzeThumosResults()

MAIN_RES_PATH = '../detections-thumos/results';
mats = dir([MAIN_RES_PATH '/*.mat']);
strD = '%1.1f';
strSep = ' \t& ';
for m = mats'
    info = load(fullfile(MAIN_RES_PATH,m.name));
    info.pr = mean(info.precision)*100;
    info.rc = mean(info.recall)*100;
    info.F = 2 * info.pr * info.rc / (info.pr+ info.rc);
    splits = strsplit(m.name,'-');
    fprintf('Resnet152- baseline 3-')
    for j  = [4 :9] 
        fprintf('%s-',splits{j});
    end
    fprintf(['%s' strSep strD strSep strD strSep strD '\\\\\n'],splits{11},info.pr,info.rc,info.F);
end
%clc;

fprintf('\n\n')
strD = '%1.1f';
strSep = ' \t| ';
id = 1;
for i = 1 : 100
    fprintf('-')
end
fprintf('\n')
for m = mats'
    info = load(fullfile(MAIN_RES_PATH,m.name));
    info.pr = mean(info.precision)*100;
    info.rc = mean(info.recall)*100;
    info.F = 2 * info.pr * info.rc / (info.pr+ info.rc);
    splits = strsplit(m.name,'-');
    fprintf('[%d] Resnet152- baseline 3-',id); id = id + 1;
    for j  = [4 :9] 
        fprintf('%s-',splits{j});
    end
    fprintf(['%s' strSep strD strSep strD strSep strD '\n'],splits{11},info.pr,info.rc,info.F);
end