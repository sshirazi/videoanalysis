function dataset = getThumosDataset(issareh,feat)
    if nargin < 1
        issareh = false;
        feat ='resnet-152';
    end
    [uvidds,anootations,pairs] = getThumosAnnotations(issareh);
    
    if issareh == 0
        datapath = '../feats-lite/';
    else
        datapath = '/short/w87/bxf660/feats-lite/';
    end
    
    dataset.bowfeatdir = fullfile(datapath,feat);
    dataset.matnames = uvidds';
    dataset.anootations = anootations;
    dataset.pairs = pairs;
end