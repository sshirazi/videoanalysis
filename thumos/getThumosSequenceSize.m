function nDimsX = getThumosSequenceSize(id,dataset)              
    matObj = matfile(fullfile(dataset.bowfeatdir,sprintf('%s-fv.mat',dataset.matnames{id})));
    info = whos(matObj,'feats');
    nDimsX = info.size(1);    
end