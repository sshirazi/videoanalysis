function annotations = getTHUMOSGTLablesForVIdeo(dataset,id)
    indx = find(dataset.anootations(:,1) == id);
    annotations = dataset.anootations(indx,[2 3 4]);
end