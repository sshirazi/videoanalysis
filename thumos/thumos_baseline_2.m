
%
% This baseline just apply rank pooling and do the matching without any
% temporal consistency search
%
%
function thumos_baseline_2(varargin)

opts.feat = 'resnet-152';
opts.issareh = false;
opts.window  = 31;
opts.stride  = 10;
opts.poolmethod = 'rank';
opts.maxdetection = 150;
opts.threshold = 0.2;
opts.prepoolnonlinear = 'none';
opts.isnci = false;
opts = bc_argparse(opts,varargin);
[~,hostname] = system('hostname');
hostname = strtrim(hostname);
opts.hostname = hostname;
STR = getOptsString(opts);
printResults();


dataset =  getThumosDataset(opts.issareh,opts.feat);  
      

pid = feature('getpid');
fid = fopen(sprintf('../logs/thumos_baseline_2/%d-w%d-d%d.log',pid,opts.window,opts.maxdetection),'a');
if opts.isnci
    MAIN_DIR = '/short/ra4/bxf660';        
    results_file = sprintf('../detections/baseline2/res%s.mat',STR);
else
    if strcmp(opts.hostname,'basura-pc')
        MAIN_DIR = '/home/basura/server/home/basura';        
    else
        MAIN_DIR = '~/';        
    end 
    results_file = sprintf('../detections-thumos/baseline2/res%s.mat',STR);
end       

DATA_FILE = sprintf('%s/THUMOS_CVPR17_Seq_Data/Feat-%s-Window%d-Stride%d-Pool%s-Rank%s.mat',...
        MAIN_DIR,opts.feat,opts.window,opts.stride,opts.prepoolnonlinear,opts.poolmethod);
    
if exist(results_file,'file')    == 2
    return;
end
    
    
matObj = matfile(DATA_FILE);
VID = matObj.VID;

NN = size(dataset.pairs,1);

THRESH = [0.1 0.2 0.3 0.4 0.5];
precision = zeros(NN,5);
recall = zeros(NN,5);

pair_id = 1;
for pair = dataset.pairs' 
    timest = tic();
    seq_id1 = pair(1);
    seq_id2 = pair(2);
    
    rankpool1 = matObj.data(find(VID == seq_id1),:);    
    gt1 = getTHUMOSGTLablesForVIdeo(dataset,seq_id1);    gt1 = gt1 .* 30;   
    
    nDimsX = getThumosSequenceSize(seq_id1,dataset);
    range1 = (opts.window-1)/2+1 : opts.stride : nDimsX -(opts.window-1)/2;    
        
    nDimsX = getThumosSequenceSize(seq_id2,dataset);
    range2 = (opts.window-1)/2+1 : opts.stride : nDimsX-(opts.window-1)/2;
    rankpool2 = matObj.data(find(VID == seq_id2),:);
    gt2 = getTHUMOSGTLablesForVIdeo(dataset,seq_id2);    gt2 = gt2 .* 30;   
        
    [pr1,pr2] = matchTemporalSequences(rankpool1,rankpool2,opts.maxdetection,opts.window, range1, range2 );
        
    for THi = 1 : numel(THRESH)
            [correctdetections,gtdetection] = eveluateTwoSequencePredictionsV2(gt1,gt2,pr1,pr2,1,THRESH(THi));            
            precision(pair_id,THi) = size(correctdetections,1)/size(gtdetection,1);
            recall(pair_id,THi) = size(correctdetections,1)/size(pr1,1);                    
    end        
      
    timest = toc(timest);
        
    fprintf('[Matching][W%d-S%d-D%d][%d][pair %d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
        opts.window,opts.stride,opts.maxdetection,pair_id,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
        mean(precision(1:pair_id,1)*100),mean(recall(1:pair_id,1)*100),timest);  
    
    fprintf(fid,'[Matching][W%d-S%d-D%d][%d][pair %d vs %d][ Prec. %1.2f][ Rec. %1.2f][Mean Pr. %1.3f][Mean Rc. %1.3f][Time %f]\n',...
        opts.window,opts.stride,opts.maxdetection,pair_id,seq_id1,seq_id2,precision(pair_id,1)*100,recall(pair_id,1)*100,...
        mean(precision(1:pair_id,1)*100),mean(recall(1:pair_id,1)*100),timest);  
    
    
    pair_id = pair_id + 1;                                          
end
save(results_file,'precision','recall','opts');  
fclose(fid);

function S = getOptsString(opt)
    opts = opt;
    opts = rmfield(opts,'issareh');
    names = fieldnames(opts);
    S = '';
    for n = names'
        value = getfield(opts, n{1});
        ss = n{1};
        S = sprintf('%s-%s',S,ss(1));
        if ischar(value)
            S = [S '-' value ];
        end
        if isnumeric(value)
            S = [S '-' num2str(value) ];
        end
    end
    fprintf('%s\n',S);
    
    function printResults()
        window = [31 61 91];
        maxdetection = [50 100 200 400];
        path = '../detections-thumos/baseline2/';
        
        for w = window
            for d = maxdetection
                spath = sprintf('%s/res-f-resnet-152-w-%d-s-10-p-rank-m-%d-t-0.2-p-none-i-h-basura-pc.mat',path,w,d);
                if exist(spath,'file') == 2
                    res = load(spath);
                    p = mean(res.precision);
                    r = mean(res.recall);
                    f =  (2 * p .* r) ./ (p +r);
                    fprintf('w-%d-d%d\t &\t %1.1f &\t %1.1f &\t %1.1f \\\\\n',w,d, p(2)*100,r(2)*100,f(2)*100);
                end
            end
        end
