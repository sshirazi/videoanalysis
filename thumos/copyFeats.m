function copyFeats()
    dataset = getThumosDataset();
    for i = 1 : numel(dataset.matnames)
        s = fullfile(dataset.bowfeatdir,sprintf('%s-fv.mat',dataset.matnames{i}));
        d = fullfile('/data/home/basura/Dataset/THUMOS2015/feats-lite',sprintf('%s-fv.mat',dataset.matnames{i}));
        copyfile(s,d);
        fprintf('[%d][%s]\n',i,dataset.matnames{i});
    end
end